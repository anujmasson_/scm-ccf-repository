#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ZS Associates'

# This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.

######################################################Module Information################################################
#   Module Name         :   MwaaCommonUtilities
#   Purpose             :   Common Utilities which will be used for MWAA Process
#   Input Parameters    :   Assumed Role ARN
#   Output              :   NA
#   Execution Steps     :   Instantiate and object of this class and call the class methods
#   Predecessor module  :   This module is specific to MWAA
#   Successor module    :   NA
#   Developed on        :   11 Nov 2021
#   Developed by        :   Sukhwinder Singh
#   Last changed on     :   NA
#   Last changed by     :   NA
#   Reason for change   :   NA
########################################################################################################################

import boto3
import traceback
import subprocess
import CommonConstants as CommonConstants
from MySQLConnectionManager import MySQLConnectionManager
from ccf_logging import logger

MODULE_NAME = "MwaaCommonUtilities"

class MwaaCommonUtilities(object):
    def __init__(self, role_arn):
        """
        :param role_arn: ARN of assumed role
        :return:
        """
        self.role_arn = role_arn

    def create_mwaa_client(self):
        """
        Create the MWAA boto3 client
        :return: MWAA Client
        """
        response = dict()
        try:
            logger.info("Creating the MWAA Client")
            sts_client = boto3.client('sts')
            assumed_role_object = sts_client.assume_role(RoleArn=self.role_arn,
                                                         RoleSessionName="AssumeRoleSession1")
            credentials = assumed_role_object['Credentials']
            mwaa_client = boto3.client('mwaa', aws_access_key_id=credentials['AccessKeyId'],
                                       aws_secret_access_key=credentials['SecretAccessKey'],
                                       aws_session_token=credentials['SessionToken'])
            logger.info("The MWAA Client has been created successfully")
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = mwaa_client
            return response

        except Exception as exp:
            error = "Failed while creating the MWAA client. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response


    def get_process_name(self, audit_db, process_id):
        """
        :param audit_db: The database name
        :param process_id: Process id for which dag need to be run
        :return: Process Name
        """
        response = dict()
        res = dict()
        try:
            logger.info("Getting the process name from cluster config table")
            process_name_query = "select {process_name}, {frequency} from {audit_db}.{cluster_config_tbl} where process_id " \
                                 " = '{process_id}'".format(process_name=CommonConstants.EMR_CLUSTER_PROCESS_COLUMN,
                                                            frequency=CommonConstants.FREQUENCY_COL,
                                                          audit_db=audit_db,
                                                          cluster_config_tbl=CommonConstants.EMR_CLUSTER_CONFIGURATION_TABLE,
                                                          process_id=process_id)
            process_name_response = MySQLConnectionManager().execute_query_mysql(process_name_query)
            if len(process_name_response) == 0 or process_name_response is None:
                raise Exception(
                    "Process_name/Frequency isn't present in cluster config table corresponding to process_id '{0}'".format(
                        process_id))
            process_name = process_name_response[CommonConstants.EMR_CLUSTER_PROCESS_COLUMN].replace(" ", "-")
            frequency = process_name_response[CommonConstants.FREQUENCY_COL]
            res["process_name"] = process_name
            res["frequency"] = frequency
            logger.info("Process name for process id '{0}' is: {1}".format(process_id, process_name))
            logger.info("Frequency for process id '{0}' is: {1}".format(process_id, frequency))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = res
            return response

        except Exception as exp:
            error = "Failed while getting the process_name and frequency for process id '{process_id}' from cluster config table. " \
                    "ERROR is: {error}".format(process_id=process_id,
                                               error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response


    def get_dataset_ids(self, audit_db, process_id):
        """
        :param audit_db: The database name
        :param process_id: Process id for which dag need to be run
        :return: List of dataset ids
        """
        response = dict()
        try:
            logger.info("Getting the dataset ids from workflow master table")
            dataset_id_query = "select {dataset_id} from {audit_db}.{workflow_master_tbl} where process_id = " \
                               " '{process_id}'".format(dataset_id = CommonConstants.DATASET_ID_COL,
                                                          audit_db = audit_db,
                                                          workflow_master_tbl = CommonConstants.EMR_PROCESS_WORKFLOW_MAP_TABLE,
                                                          process_id = process_id)

            dataset_id_response = MySQLConnectionManager().execute_query_mysql(dataset_id_query)
            if len(dataset_id_response) == 0 or dataset_id_response is None:
                raise Exception("Dataset ids aren't present in workflow master table corresponding to process_id '{0}'".format(process_id))
            dataset_ids = [x[CommonConstants.DATASET_ID_COL] for x in dataset_id_response if CommonConstants.DATASET_ID_COL in x.keys()]
            logger.info("The Dataset id(s) for process id {0} are: {1}".format(process_id, dataset_ids))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = dataset_ids
            return response

        except Exception as exp:
            error = "Failed while getting the dataset ids for process id '{process_id}' from workflow master table. " \
                    "ERROR is: {error}".format(process_id=process_id, error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def get_rule_configs(self, audit_db, process_id):
        """

        """
        response = dict()
        try:
            logger.info("Getting the rule configs from rule config table")
            rule_config_query = "select {step_name}, {frequency}, {upstream_dependency} from {audit_db}.{rule_config_tbl} where process_id = " \
                               " '{process_id}'".format(step_name=CommonConstants.STEP_NAME_LOG_KEY,
                                                        frequency=CommonConstants.FREQUENCY_LOG_KEY,
                                                        upstream_dependency=CommonConstants.UPSTREAM_DEPENDENCY_KEY,
                                                        audit_db=audit_db,
                                                        rule_config_tbl=CommonConstants.RULE_CONFIG_TABLE,
                                                        process_id=process_id)

            rule_config_response = MySQLConnectionManager().execute_query_mysql(rule_config_query)
            if len(rule_config_response) == 0 or rule_config_response is None:
                raise Exception(
                    "Rules aren't present in rule config table corresponding to process_id '{0}'".format(
                        process_id))
            logger.info("The rule configs are: {0}".format(rule_config_response))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = rule_config_response
            return response
        except Exception as exp:
            error = "Failed while getting the rule configurations for process id '{process_id}' from rule config table. " \
                    "ERROR is: {error}".format(process_id=process_id,
                                               error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def provide_webserver_link(self, airflow_env_name):
        """
        Get the webserver link of MWAA UI
        :param airflow_env_name: Name of the airflow environment
        :return:
        """
        response = dict()
        try:
            logger.info("Getting the webserver link of MWAA UI")
            mwaa_client_response = self.create_mwaa_client()
            if mwaa_client_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(mwaa_client_response)
            mwaa_client = mwaa_client_response[CommonConstants.RESULT_KEY]
            response = mwaa_client.get_environment(Name=airflow_env_name)
            webserver_link = response['Environment']['WebserverUrl']
            logger.info("The webserver link of MWAA UI is: {0}".format(webserver_link))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = webserver_link
            return response
        except Exception as exp:
            error = "Failed while getting the webserver link of MWAA. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def execute_shell_command(self, command):
        """
        Execute the Shell command
        :param airflow_env_name: Name of the airflow environment
        :return:
        """
        return_status = dict()
        try:
            status_message = "Started executing shell command " + command
            logger.info(status_message)
            command_output = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            standard_output, standard_error = command_output.communicate()
            if standard_output:
                standard_output_line = standard_output.decode("utf-8")
            elif standard_error:
                standard_error_line = standard_error.decode("utf-8")
            if command_output.returncode == 0:
                return_status[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
                return_status[CommonConstants.RESULT_KEY] = standard_output_line
                return return_status
            else:
                return_status[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
                return_status[CommonConstants.ERROR_KEY] = "The command '{0}' has failed. ".format(command) + \
                                str(standard_error_line)
                return return_status
        except Exception as exp:
            error = "Failed while executing the shell command '{command}'. " \
                    "ERROR is: {error}".format(command=command, error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            return_status[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            return_status[CommonConstants.RESULT_KEY] = "Error"
            return_status[CommonConstants.ERROR_KEY] = error
            return return_status


