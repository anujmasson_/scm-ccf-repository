#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ZS Associates'

# This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.

######################################################Module Information################################################
#   Module Name         :   MwaaRunDagUtility
#   Purpose             :   Trigger the dag in MWAA
#   Input Parameters    :   process_id, airflow_env_name, assumed_role_arn
#   Output              :   NA
#   Execution Steps     :   Instantiate and object of this class and call the class methods
#   Predecessor module  :   This module is specific to MWAA
#   Successor module    :   NA
#   Developed on        :   11 Nov 2021
#   Developed by        :   Sukhwinder Singh
#   Last changed on     :   NA
#   Last changed by     :   NA
#   Reason for change   :   NA
########################################################################################################################

import base64
import requests
from datetime import datetime
import traceback
import argparse
from ConfigUtility import JsonConfigUtility
import CommonConstants as CommonConstants
from MwaaCommonUtilities import MwaaCommonUtilities
from ccf_logging import logger

MODULE_NAME = "MwaaRunDagUtility"


class RunDag():
    def __init__(self, process_id, airflow_env_name, assumed_role_arn):
        """

        :param process_id: Process id for which dag need to be run
        :param airflow_env_name: MWAA Environment Name
        :param assumed_role_arn: ARN of assumed role
        :return: Webserver link
        """
        self.process_id = process_id
        self.airflow_env_name = airflow_env_name
        self.assumed_role_arn = assumed_role_arn
        logger.info("The provided process_id is: {0}".format(process_id))
        logger.info("The provided airflow_env_name is: {0}".format(airflow_env_name))
        logger.info("The provided assumed_role_arn is: {0}".format(assumed_role_arn))
        self.configuration = JsonConfigUtility(
            CommonConstants.AIRFLOW_CODE_PATH + '/' + CommonConstants.ENVIRONMENT_CONFIG_FILE)
        self.mysql_database = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
        self.aws_region = self.configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, "aws_region"])
        self.utility_obj = MwaaCommonUtilities(role_arn=self.assumed_role_arn)
        # self.logger = self.utility_obj.get_logger()

    def trigger_dag(self):
        """
        It will trigger the dag
        """
        response = dict()
        try:
            # Getting the process name for provided process id
            process_name_response = self.utility_obj.get_process_name(audit_db=self.mysql_database,
                                                                      process_id=self.process_id)
            if process_name_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(process_name_response)
            process_name = process_name_response[CommonConstants.RESULT_KEY]["process_name"]

            # Creating the MWAA client
            mwaa_client_response = self.utility_obj.create_mwaa_client()
            if mwaa_client_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(mwaa_client_response)
            mwaa_client = mwaa_client_response[CommonConstants.RESULT_KEY]

            # Creating the MWAA CLI Token
            token_response = mwaa_client.create_cli_token(Name=self.airflow_env_name)
            run_id = "trigger__{0}".format(datetime.now()).replace(" ", "T")
            mwaa_command = "dags trigger -r " + run_id + " " + process_name
            endpoint = token_response["WebServerHostname"]
            jwt_token = token_response["CliToken"]
            mwaa_cli_endpoint = 'https://{0}/aws_mwaa/cli'.format(endpoint)

            # Triggering the MWAA Dag
            mwaa_response = requests.post(mwaa_cli_endpoint,
                                          headers={'Authorization': 'Bearer ' + jwt_token,
                                                   'Content-Type': 'text/plain'},
                                          data=mwaa_command)
            logger.info(base64.b64decode(mwaa_response.json()['stderr']).decode('utf8'))
            logger.info(base64.b64decode(mwaa_response.json()['stdout']).decode('utf8'))
            webserver_link_response = self.utility_obj.provide_webserver_link(self.airflow_env_name)
            if webserver_link_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(webserver_link_response)
            webserver_link = CommonConstants.RESULT_KEY
            return webserver_link

        except Exception as exp:
            error = "Failed while triggering the dag in mwaa. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            raise exp

if __name__ == '__main__':
    try:
        PARSER = argparse.ArgumentParser(description="MWAA RUN DAG")
        PARSER.add_argument("-p", "--process_id", help="Process id for which dag need to be run", required=True)
        PARSER.add_argument("-ae", "--airflow_environment", help="MWAA Environment Name", required=True)
        PARSER.add_argument("-ra", "--role_arn", help="ARN of assumed role", required=True)
        ARGS = vars(PARSER.parse_args())
        PROCESS_ID = ARGS['process_id']
        AIRFLOW_ENVIRONMENT = ARGS['airflow_environment']
        ROLE_ARN = ARGS['role_arn']
        obj = RunDag(process_id=PROCESS_ID, airflow_env_name=AIRFLOW_ENVIRONMENT, assumed_role_arn=ROLE_ARN)
        obj.trigger_dag()
    except Exception as exp:
        raise Exception
