# This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
"""This module fetches batch status."""
import ast
import copy
import datetime as dt
import json
import logging
import os
import random
# !/usr/bin/python3
# -*- coding: utf-8 -*-
import socket
import subprocess
import sys
import time
import traceback
from urllib.parse import urlparse

import boto3

import CommonConstants
from CommonUtils import CommonUtils
from ConfigUtility import JsonConfigUtility
from CustomS3Utility import CustomS3Utility
from DWLineageWrapper import DWLineageWrapper
from EmrClusterLaunchWrapper import EmrClusterLaunchWrapper
from Get_EMR_Host_IP import Get_EMR_Host_IP
from IngestionLineageWrapper import IngestionLineageWrapper
from MySQLConnectionManager import MySQLConnectionManager
from NotificationUtility import NotificationUtility
from PopulateDependency import PopulateDependency
from SystemManagerUtility import SystemManagerUtility

sys.path.insert(0, os.getcwd())

logger = logging.getLogger('__name__')
hndlr = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hndlr.setFormatter(formatter)


def fetch_batch_status(batch_id, dataset_ids, process_id, cluster_id):
    """This method fetched batch status"""
    logger.info("Starting function to retrieve the batch status")
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "select process_id, batch_id, dataset_id, batch_status," \
            " cast(batch_start_time as CHAR) as batch_start_time," \
            " cast(batch_end_time as CHAR) as batch_end_time from " + audit_db + \
            "." + CommonConstants.BATCH_TABLE + " where dataset_id in (" + str(
                dataset_ids) + ") and batch_id in (" + str(batch_id) + \
            ") and process_id = " + str(process_id) + " and cluster_id = '" + str(cluster_id) + \
            "' order by batch_start_time"
    logger.info("Query to retrieve the batch status: " + query)
    query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output" + str(query_output))
    if "FAIL" in str(query_output).upper():
        final_batch_status = CommonConstants.STATUS_FAILED.upper()
    elif CommonConstants.IN_PROGRESS_DESC.upper() in str(query_output).upper():
        final_batch_status = CommonConstants.IN_PROGRESS_DESC.upper()
    elif CommonConstants.STATUS_RUNNING.upper() in str(query_output).upper():
        final_batch_status = CommonConstants.IN_PROGRESS_DESC.upper()
    else:
        final_batch_status = CommonConstants.STATUS_SUCCEEDED
    return {"final_batch_status": final_batch_status, "batch_dtls": query_output}


def fetch_batch_id(cluster_id, process_id):
    """This method fetches batch id"""
    logger.info("Starting function to retrieve the Batch ID of current execution")
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "select group_concat(batch_id separator ',') as batch_id from " + audit_db + \
            "." + CommonConstants.BATCH_TABLE + " where process_id = " + str(
                process_id) + " and cluster_id = '" + str(cluster_id) + "'"
    logger.info("Query to retrieve the batch ID: " + query)
    query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output : " + str(query_output))
    return query_output[0]["batch_id"]


def generate_batch_status_html(batch_status):
    """This function generates batch status html"""
    logger.info("Starting function to generate batch status HTML Table")
    logger.info("Batch Status recieved: " + str(batch_status))
    dtls_log = batch_status["batch_dtls"]

    html_table_header = '"<table class="tg" align="center"><tr><th class="tg-yw4l">process_id' \
                        '</th><th class="tg-yw4l">batch_id</th><th class="tg-yw4l">' \
                        'dataset_id</th><th class="tg-yw4l">batch_status</th>' \
                        '<th class="tg-yw4l">batch_start_time</th>' \
                        '<th class="tg-yw4l">batch_end_time</th></tr>'
    html_table_end = "</table>"
    final_row = ""
    logger.info("Parsing each record from the status")
    for var in dtls_log:
        row = dict(var)
        row_str = ('<td class="tg-yw4l">' + str(row["process_id"]
                                                ) + '</td>').lstrip('"').rstrip('"') + \
                  ('<td class="tg-yw4l">' + str(row["batch_id"]
                                                ) + '</td>').lstrip('"').rstrip('"') + \
                  ('<td class="tg-yw4l">' + str(row["dataset_id"]
                                                ) + '</td>').lstrip('"').rstrip('"') + \
                  ('<td class="tg-yw4l">' + str(row["batch_status"]
                                                ) + '</td>').lstrip('"').rstrip('"') + \
                  ('<td class="tg-yw4l">' + str(row["batch_start_time"]
                                                ) + '</td>').lstrip('"').rstrip('"') + \
                  ('<td class="tg-yw4l">' + str(row["batch_end_time"]
                                                ) + '</td>').lstrip('"').rstrip('"')
        row_str = (row_str + "</tr>").lstrip('"').rstrip('"')
        final_row = final_row + row_str

    html_table = (html_table_header + final_row + html_table_end).lstrip('"').rstrip('"')
    logger.info("HTML Spec generated for the Batch load details" + str(html_table))
    return html_table


def fetch_dqm_status(batch_id, dataset_ids):
    """This method fetches dqm status"""
    logger.info("Starting function to retrieve the DQM Summary status")
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "select application_id, dataset_id, batch_id, file_id, column_name," \
            " qc_id,  error_count, " \
            "qc_status, qc_message from " + audit_db + ".log_dqm_smry where dataset_id in (" + \
            str(dataset_ids) + ") and batch_id in (" + str(batch_id) + ")"
    logger.info("Query to retrieve the DQM status: " + query)
    query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output" + str(query_output))
    if query_output:
        return {"dqm_dtls": query_output}
    else:
        return {"dqm_dtls": ""}


def generate_dqm_status_html(dqm_status):
    """This method generates dqm status html"""
    logger.info("DQM Status retrieved: " + str(dqm_status))
    dqm_log = dqm_status["dqm_dtls"]
    if dqm_log:
        html_table_header = '"<table class="tg" align="center"><tr>' \
                            '<th class="tg-yw4l">application_id</th>' \
                            '<th class="tg-yw4l">dataset_id</th>' \
                            '<th class="tg-yw4l">batch_id</th>' \
                            '<th class="tg-yw4l">file_id</th>' \
                            '<th class="tg-yw4l">column_name</th>' \
                            '<th class="tg-yw4l">error_count</th>' \
                            '<th class="tg-yw4l">qc_status</th>' \
                            '<th class="tg-yw4l">qc_message</th>' \
                            '</tr>'
        html_table_end = "</table>"
        final_row = ""
        for var in dqm_log:
            row = dict(var)
            row_str = ('<td class="tg-yw4l">' + str(row["application_id"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["dataset_id"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["batch_id"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["file_id"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["column_name"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["error_count"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["qc_status"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["qc_message"]) + '</td>'
                       ).lstrip('"').rstrip('"')
            row_str = (row_str + "</tr>").lstrip('"').rstrip('"')
            final_row = final_row + row_str
        html_table = (html_table_header + final_row + html_table_end).lstrip('"').rstrip('"')
    else:
        html_table = ""
    logger.info("HTML Spec generated for the DQM details" + str(html_table))
    return html_table


def get_dataset_ids(process_id):
    """This method get dataset ids"""
    """
    :param process_id: Process ID as configured in the table
    :return: data set id's list
    """
    logger.info("Starting function to get the Dataset IDs based on process ID recieved: " +
                str(process_id))
    configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                   CommonConstants.ENVIRONMENT_CONFIG_FILE))

    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])

    dataset_id_list = []
    try:

        query = "Select distinct(" + CommonConstants.MYSQL_DATASET_ID + ") from " + audit_db + "." \
                + CommonConstants.EMR_PROCESS_WORKFLOW_MAP_TABLE + " where " + \
                CommonConstants.EMR_PROCESS_WORKFLOW_COLUMN + "='" + str(
                    process_id) + "' and " + CommonConstants.EMR_PROCESS_WORKFLOW_ACTIVE_COLUMN + \
                "='" + str(
                    CommonConstants.WORKFLOW_DATASET_ACTIVE_VALUE) + "'"
        logger.info("Query to retrieve the dataset ID: " + str(query))
        dataset_ids = MySQLConnectionManager().execute_query_mysql(query, False)
        logger.info("Output of query: " + str(dataset_ids))
        for id in dataset_ids:
            dataset_id_list.append(str(int(id[CommonConstants.MYSQL_DATASET_ID])))
        logger.info("List of Dataset IDs  extracted: " + str(dataset_id_list))
        return dataset_id_list
    except Exception as exception:
        logger.error(str(traceback.format_exc()))
        logger.error(str(exception))
        raise exception


def fetch_ingestion_email_dqm_info(process_id, dataset_id):
    """This method fetches ingestion email dqm information"""
    logger.info("Starting function to retrieve DQM email information for Process ID: " + str(
        process_id) +
                " and dataset ID: " + str(dataset_id))
    replace_dict = {}

    logger.info("Retrieving Cluster ID for the Process ID: " + str(process_id))

    cluster_id = get_cluster_id(process_id)
    # Fetching Batch status since Batch ID is assigned
    logger.info("Retrieving Batch ID based on Cluster ID - " + str(cluster_id
                                                                   ) + " configured Process ID: " +
                str(process_id) + " and configured Dataset ID - " + str(dataset_id))
    batch_id = fetch_batch_id(cluster_id, process_id)

    if batch_id:
        logger.info("Batch ID present hence retrieving detailed status")
        batch_id_str = batch_id.replace(",", ",\n")
        replace_dict["batch_id"] = batch_id_str
        logger.info("Batch ID: " + str(batch_id_str))

        batch_status = fetch_batch_status(batch_id, dataset_id, process_id, cluster_id)
        dqm_status = fetch_dqm_status(batch_id, dataset_id)
        html_spec = generate_dqm_status_html(dqm_status)

        replace_dict["status"] = batch_status["final_batch_status"]
        replace_dict["detailed_status"] = html_spec

    else:
        logger.info("Batch ID is Not Assigned however the Cluster launch is successful. "
                    "Hence marking status of run as failed as process failed at Prelanding step")
        replace_dict["batch_id"] = "Not Assigned"
        replace_dict["status"] = CommonConstants.STATUS_FAILED
        replace_dict["detailed_status"] = ""

    logger.info("Final DQM details extracted for the email: " + str(replace_dict))
    return replace_dict


def fetch_ingestion_email_batch_info(process_id, dataset_id):
    """This method fetches ingestion email batch information"""
    logger.info("Starting function to retrieve Batch Ingestion email information for Process"
                " ID: " + str(process_id) +
                " and dataset ID: " + str(dataset_id))
    replace_dict = {}

    logger.info("Retrieving Cluster ID for the Process ID: " + str(process_id))
    cluster_id = get_cluster_id(process_id)
    # Fetching Batch status since Batch ID is assigned
    logger.info("Retrieving Batch ID based on Cluster ID - " + str(cluster_id) + " configured "
                                                                                 "Process ID: " +
                str(process_id) + " and configured Dataset ID - " + str(dataset_id))
    batch_id = fetch_batch_id(cluster_id, process_id)

    if batch_id:
        batch_id_str = batch_id.replace(",", ",\n")
        replace_dict["batch_id"] = batch_id_str
        logger.info("Batch ID: " + str(batch_id_str))
        batch_status = fetch_batch_status(batch_id, dataset_id, process_id, cluster_id)
        html_spec = generate_batch_status_html(batch_status)
        replace_dict["status"] = batch_status["final_batch_status"]
        replace_dict["detailed_status"] = html_spec

    else:
        logger.info("Batch ID is Not Assigned however the Cluster launch is successful. "
                    "Hence marking status of run as failed as process failed at Prelanding step")
        replace_dict["batch_id"] = "Not Assigned"
        replace_dict["status"] = CommonConstants.STATUS_FAILED
        replace_dict["detailed_status"] = ""
    logger.info("Final Batch Ingestion details extracted for the email: " + str(replace_dict))
    return replace_dict


def fetch_ingestion_email_common_info(process_id, dag_name):
    """This method fetches ingestion email common information"""
    logger.info("Starting function to retrieve Common Ingestion email information for"
                " Process ID: " + str(process_id) +
                " and DAG Name: " + str(dag_name))
    configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                   CommonConstants.ENVIRONMENT_CONFIG_FILE))
    common_metadata = {"process_id": str(process_id), "dag_name": str(dag_name)}
    # ***** Mandatory Information Reported in every email *****
    # 1. Process ID
    # 2. Dataset Names
    # 3. DAG Name
    # 4. Batch ID
    # 5. Status

    # Retrieving the Airflow Link
    hostname = socket.gethostname()
    ip_address = socket.gethostbyname(hostname)

    airflow_url = "http://" + ip_address + ":8080/admin/airflow/graph?root=&dag_id=" + str(dag_name)

    logger.info("Airflow URL Generated:" + str(airflow_url))
    common_metadata["airflow_link"] = str(airflow_url)
    # Retrieving Environment

    env = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "environment"])
    logger.info("Environment retrieved from Configurations is: " + str(env))
    common_metadata["env"] = str(env)

    # Fetching the dataset names from the Control tables

    dataset_id_list = get_dataset_ids(process_id)
    final_dataset_id = ",".join(map(str, dataset_id_list))

    logger.info("List of Dataset IDs to report status in this email: " + str(final_dataset_id))

    configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                   CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "select group_concat(dataset_name separator ',') as dataset_names from " + audit_db + \
            ".ctl_dataset_master where dataset_id in (" + final_dataset_id + ")"
    logger.info("Query to retrieve the names of configured Dataset IDs: " + query)
    query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output" + str(query_output))
    dataset_names = query_output[0]['dataset_names']
    common_metadata["dataset_name"] = str(dataset_names)
    common_metadata["dataset_id"] = str(final_dataset_id)
    logger.info("Final Batch Ingestion common information extracted for the email: " + str(
        common_metadata))
    return common_metadata


def get_cycle_step_status(cycle_id):
    """This method gets cycle step status"""
    logger.info("Starting function to retrieve the batch status")
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "select process_id, frequency, cycle_id, step_name, step_status, cast(" \
            "step_start_time as CHAR)" \
            " as step_start_time, cast(step_end_time as CHAR) as step_end_time from " + audit_db + \
            "." + CommonConstants.LOG_STEP_DTL + " where cycle_id = '" + str(cycle_id) + \
            "' order by step_start_time"
    logger.info("Query to retrieve the cycle steps status: " + query)
    query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output" + str(query_output))

    query = "select cycle_status from " + audit_db + \
            "." + CommonConstants.LOG_CYCLE_DTL + " where cycle_id = " + str(cycle_id)
    logger.info("Query to retrieve the cycle status: " + query)
    cycle_query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output" + str(cycle_query_output))
    final_cycle_status = cycle_query_output[0]["cycle_status"]
    return {"status": final_cycle_status, "cycle_dtls": query_output}


def generate_cycle_status_html(cycle_status):
    """This method generates cycle status html"""
    logger.info("Starting function to generate cycle status HTML Table")
    logger.info("Cycle Status recieved: " + str(cycle_status))
    dtls_log = cycle_status["cycle_dtls"]
    if dtls_log:
        html_table_header = '"<table class="tg" align="center"><tr><th class="tg-yw4l">process_id' \
                            '</th><th class="tg-yw4l">frequency</th><th class="tg-yw4l">' \
                            'cycle_id</th><th class="tg-yw4l">step_name</th><th class="tg-yw4l">' \
                            'step_status</th>' \
                            '<th class="tg-yw4l">step_start_time</th><th class="tg-yw4l">' \
                            'step_end_time</th></tr>'
        html_table_end = "</table>"
        final_row = ""
        logger.info("Parsing each record from the status")
        for var in dtls_log:
            row = dict(var)
            row_str = ('<td class="tg-yw4l">' + str(row["process_id"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["frequency"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["cycle_id"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["step_name"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["step_status"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["step_start_time"]) + '</td>'
                       ).lstrip('"').rstrip('"') + \
                      ('<td class="tg-yw4l">' + str(row["step_end_time"]) + '</td>'
                       ).lstrip('"').rstrip('"')
            row_str = (row_str + "</tr>").lstrip('"').rstrip('"')
            final_row = final_row + row_str

        html_table = (html_table_header + final_row + html_table_end).lstrip('"').rstrip('"')
        logger.info("HTML Spec generated for the Cycle details" + str(html_table))
        return html_table
    else:
        return ""


def fetch_dw_email_cycle_info(process_id):
    """This method fetches dw email cycle information"""
    logger.info("Starting function to fetch the cycle information for DW email")
    email_details = {}
    cycle_details = get_cycle_details(process_id, cycle_assigned_flag=True)
    logger.info("Cycle Details retrieved: " + str(cycle_details))
    email_details = copy.deepcopy(cycle_details)
    step_details = get_cycle_step_status(cycle_details["cycle_id"])
    email_details["detailed_status"] = generate_cycle_status_html(step_details)
    email_details["status"] = step_details["status"]
    logger.info("Final cycle information retrieved for DW email: " + str(email_details))
    return email_details


def fetch_dw_email_common_info(process_id, dag_name):
    """This method fetches dw email common information"""
    logger.info("Starting function to retrieve Common Ingestion email information for "
                "Process ID: " + str(process_id) +
                " and DAG Name: " + str(dag_name))
    configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                   CommonConstants.ENVIRONMENT_CONFIG_FILE))
    common_metadata = {"process_id": str(process_id), "dag_name": str(dag_name)}
    # ***** Mandatory Information Reported in every email *****
    # 1. Process ID
    # 2. Cycle ID
    # 3. DAG Name
    # 4. DAG URL
    # 5. Data Date
    # 6. Status

    # Retrieving the Airflow Link
    hostname = socket.gethostname()
    ip_address = socket.gethostbyname(hostname)

    airflow_url = "http://" + ip_address + ":8080/admin/airflow/graph?root=&dag_id=" + str(dag_name)

    logger.info("Airflow URL Generated:" + str(airflow_url))
    common_metadata["airflow_link"] = str(airflow_url)
    # Retrieving Environment

    env = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "environment"])
    logger.info("Environment retrieved from Configurations is: " + str(env))
    common_metadata["env"] = str(env)

    # Fetching the dataset names from the Control tables
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
    #  "mysql_db"])
    # Return - Cycle ID, Frequency, Data date
    cycle_details = get_cycle_details(process_id, cycle_assigned_flag=False)
    common_metadata["data_date"] = str(cycle_details["data_date"])
    logger.info("Final DW common information extracted for the email: " + str(common_metadata))
    return common_metadata


def trigger_notification_utility(email_type, process_id, dag_name, emr_status=None, **kwargs):
    """This method triggers notification utility"""
    try:
        replace_variables = {}
        logger.info("Staring function to trigger Notification Utility")

        logger.info("Preparing the common information to be sent in all emails")
        configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                       CommonConstants.ENVIRONMENT_CONFIG_FILE))

        email_types = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                       "email_type_configurations"])
        if email_types:
            email_types = list(email_types.keys())
            if email_type in email_types:
                logger.info("Email type: " + str(email_type) + " is configured")
            else:
                logger.info("Email type: " + str(email_type) + " is not configured, cannot proceed")
                raise Exception("Email type: " + str(email_type) + " is not configured,"
                                                                   " cannot proceed")
        else:
            raise Exception("No email types configured, cannot proceed")

        notification_flag = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                             "email_type_configurations",
                                                             str(email_type),
                                                             "notification_flag"])

        logger.info("Notification Flag: " + str(notification_flag))

        if notification_flag.lower() != 'y':
            logger.info("Notification is disabled hence skipping notification"
                        " trigger and exiting function")

        else:
            logger.info("Notification is enabled hence starting notification trigger")

            # Common configurations

            email_ses_region = configuration.get_configuration(
                [CommonConstants.ENVIRONMENT_PARAMS_KEY, "ses_region"])
            logger.info("SES Region retrieved from Configurations is: " + str(email_ses_region))
            email_template_path = configuration.get_configuration(
                [CommonConstants.ENVIRONMENT_PARAMS_KEY, "email_template_path"])
            logger.info("Email template path retrieved from Configurations is: " + str(
                email_template_path))

            # Email type specific configurations

            email_sender = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                            "email_type_configurations",
                                                            str(email_type),
                                                            "ses_sender"])
            logger.info("Email Sender retrieved from Configurations is: " + str(email_sender))

            email_recipient_list = configuration.get_configuration(
                [CommonConstants.ENVIRONMENT_PARAMS_KEY, "email_type_configurations",
                 str(email_type), "ses_recipient_list"])
            logger.info("Email Recipient retrieved from Configurations is: " + str(
                email_recipient_list))

            email_subject = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY,
                                                             "email_type_configurations",
                                                             str(email_type),
                                                             "subject"])
            logger.info("Email Subject retrieved from Configurations is: " + str(email_subject))

            email_template_name = configuration.get_configuration(
                [CommonConstants.ENVIRONMENT_PARAMS_KEY, "email_type_configurations",
                 str(email_type), "template_name"])
            logger.info("Email Template Name retrieved from Configurations is: "
                        + str(email_template_name))

            email_template_path = (email_template_path + "/" + email_template_name
                                   ).replace("//", "/")
            logger.info("Final Email template path is: " + str(email_template_path))

            if email_type == "batch_status" or email_type == "dqm":
                common_info = fetch_ingestion_email_common_info(str(process_id), dag_name)
                replace_variables = copy.deepcopy(common_info)
                logger.info("Common email information for Batch/DQM" + str(replace_variables))
            elif email_type == "cycle_status":
                common_info = fetch_dw_email_common_info(str(process_id), dag_name)
                replace_variables = copy.deepcopy(common_info)
                logger.info("Common Email information for DW: " + str(replace_variables))
            replace_variables["status"] = CommonConstants.STATUS_NOT_STARTED
            logger.info("Variables prepared: " + str(replace_variables))

            if str(emr_status) == "None":
                replace_variables["detailed_status"] = ""
                replace_variables["batch_id"] = "Not Assigned"
                replace_variables["cycle_id"] = "Not Assigned"
                logger.info("Either failed during EMR launch or Cluster is not yet launched, "
                            "hence Batch/Cycle ID will be unassigned")
                logger.info("Variables prepared: " + str(replace_variables))
            else:
                replace_variables["status"] = str(emr_status)
                logger.info("Variables prepared: " + str(replace_variables))
                if str(emr_status) == CommonConstants.STATUS_SUCCEEDED:
                    if email_type == "batch_status":
                        dynamic_variables = fetch_ingestion_email_batch_info(
                            str(process_id), replace_variables["dataset_id"])
                    elif email_type == "dqm":
                        dynamic_variables = fetch_ingestion_email_dqm_info(
                            str(process_id), replace_variables["dataset_id"])
                    elif email_type == "cycle_status":
                        dynamic_variables = fetch_dw_email_cycle_info(str(process_id))
                    else:
                        #### Add other email type conditionals here and call the functions to
                        # fetch the variables for that email type
                        dynamic_variables = {}
                    replace_variables.update(dynamic_variables)
                    logger.info(str(replace_variables))
                else:
                    replace_variables["detailed_status"] = ""
                    replace_variables["batch_id"] = "Not Assigned"
                    replace_variables["cycle_id"] = "Not Assigned"
                    logger.info(
                        "Failed during EMR launch, hence Batch/Cycle ID will be unassigned")
                    logger.info("Variables prepared: " + str(replace_variables))

            logger.info("Final list of replacement variables for Email: " + str(replace_variables))

            # Preparing email details
            notif_obj = NotificationUtility()
            output = notif_obj.send_notification(email_ses_region, email_subject, email_sender,
                                                 email_recipient_list,
                                                 email_template=email_template_path,
                                                 replace_param_dict=replace_variables)
            logger.info("Output of Email Notification Trigger: " + str(output))

    except Exception as exc:
        logger.error(str(traceback.format_exc()))
        logger.error("Failed while triggering Notification Utility. Error: " + exc.message)


def execute_shell_command(command):
    """This method executes shell command"""
    status_message = ""
    try:
        status_message = "Started executing shell command " + command
        logger.info(status_message)
        command_output = subprocess.Popen(command, stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE, shell=True)
        standard_output, standard_error = command_output.communicate()
        logger.info(standard_output)
        logger.info(standard_error)
        if command_output.returncode == 0:
            return True
        else:
            status_message = "Error occurred while executing command on shell :" + command
            raise Exception(status_message)
    except Exception as exc:
        logger.error(str(traceback.format_exc()))
        logger.error(status_message)
        raise exc


def get_cluster_id(process_id, **kwargs):
    """This method gets cluster id"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "Select cluster_id from {audit_db}.{emr_cluster_details_table} where " \
            "process_id='{process_id}' and cluster_status='{state}'".format(
                audit_db=audit_db,
                emr_cluster_details_table=CommonConstants.EMR_CLUSTER_DETAILS_TABLE,
                process_id=process_id, state=CommonConstants.CLUSTER_ACTIVE_STATE)
    logger.info("Query to retrieve the Cluster ID: " + str(query))
    cluster_ids = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Result: " + str(cluster_ids))

    # TODO: Handle cases where blank is returned by MySQL
    return cluster_ids[0]['cluster_id']


def get_spark_configurations(dataset_id, process_id, **kwargs):
    """This method gets spark configurations"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "Select spark_config from {audit_db}.{emr_process_workflow_map_table} where " \
            "dataset_id='{dataset_id}' and process_id = '{process_id}'".format(
                audit_db=audit_db,
                emr_process_workflow_map_table=CommonConstants.EMR_PROCESS_WORKFLOW_MAP_TABLE,
                dataset_id=dataset_id, process_id=process_id)
    logger.info("Query to retrieve the Spark Configurations: " + str(query))
    spark_config = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Result: " + str(spark_config))
    if str(spark_config) == None or str(spark_config) == 'NULL':
        return ''
    else:
        return spark_config[0]['spark_config']
        # TODO: Handle cases where blank is returned by MySQL


def get_host(process_id, instance_group_list, **kwargs):
    """This method gets host"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    logger.info("The Audit Database name extracted from configuration: " + str(audit_db))
    ip_list = []
    query = "Select cluster_id from {audit_db}.{emr_cluster_details_table} where " \
            "process_id='{process_id}' and cluster_status='{state}'".format(
                audit_db=audit_db,
                emr_cluster_details_table=CommonConstants.EMR_CLUSTER_DETAILS_TABLE,
                process_id=process_id,
                state=CommonConstants.CLUSTER_ACTIVE_STATE)
    logger.info("Query for retrieving the Cluster host name: " + str(query))
    cluster_ids = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Result: " + str(cluster_ids))
    if len(cluster_ids) > 1:
        raise Exception("More then one clusters are in Waiting state")
    elif len(cluster_ids) == 0:
        raise Exception("No Cluster in Waiting state")
    else:
        emrClusterLaunch = EmrClusterLaunchWrapper()
        cluster_configs = emrClusterLaunch.extractClusterConfiguration(process_id)
        logger.info("Cluster Configuration Provided : ", cluster_configs[0])
        cluster_configs = cluster_configs[0]
        region_string = str(cluster_configs["region_name"])
        logger.info("Cluster ID retrieved: " + str(cluster_ids[0]["cluster_id"]))
        cluster_id_str = str(cluster_ids[0]["cluster_id"])
        obj = Get_EMR_Host_IP()
        ip_list = obj.get_running_instance_ips(cluster_id_str, region_string,
                                               instance_group_list)
        logger.info("List of IP addresses retrived for instances: " + str(instance_group_list) + " is: " + str(ip_list))
        random_selection = random.choice(ip_list)
        if random_selection is None:
            query = "Select host_ip_address from {audit_db}.{emr_cluster_host_details} where  cluster_id='{cluster_id}'".format(
                audit_db=audit_db, emr_cluster_host_details=CommonConstants.EMR_CLUSTER_HOST_DETAILS,
                cluster_id=cluster_ids[0]['cluster_id'])
            cluster_ips = MySQLConnectionManager().execute_query_mysql(query, False)
            for ip in cluster_ips:
                ip_list.append(ip['host_ip_address'])
        random_selection = random.choice(ip_list)
        logger.info("IP address selected randomly: " + str(random_selection))
    return random_selection


def launch_emr(process_id, **kwargs):
    """This method launches emr"""
    kwargs["ti"].xcom_push(key='emr_status', value=CommonConstants.STATUS_FAILED)
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    logger.info(audit_db)
    emrClusterLaunch = EmrClusterLaunchWrapper()
    cluster_configs = emrClusterLaunch.extractClusterConfiguration(process_id)
    logger.info("Cluster Configuration Provided >> ", cluster_configs[0])
    cluster_configs = cluster_configs[0]
    property_json = json.loads(cluster_configs["property_json"])
    query = "Select cluster_id from {audit_db}.{emr_cluster_details_table} where " \
            "process_id={process_id} and cluster_status='{state}'".format(
                audit_db=audit_db,
                emr_cluster_details_table=CommonConstants.EMR_CLUSTER_DETAILS_TABLE,
                process_id=process_id,
                state=CommonConstants.CLUSTER_ACTIVE_STATE)
    logger.info(query)
    cluster_ids = MySQLConnectionManager().execute_query_mysql(query, False)
    if CommonConstants.CHECK_LAUNCH_COLUMN not in property_json:
        if cluster_ids:
            kwargs["ti"].xcom_push(key='emr_status', value=CommonConstants.STATUS_FAILED)
            raise Exception("More than one clusters are in Waiting state")
        else:
            emrClusterLaunch.main(process_id, "start")
            # launch_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
            #                                          "EmrClusterLaunchWrapper.py") + " -p " + str(
            #     process_id) + " -a start"
            # execute_shell_command(launch_string)
    else:
        if property_json[CommonConstants.CHECK_LAUNCH_COLUMN
        ] == CommonConstants.CHECK_LAUNCH_ENABLE:
            status_message = "Check flag is present with 'Y' value.Proceeding with existing " \
                             "cluster Id in WAITING state"
            logger.info(status_message)
        elif property_json[CommonConstants.CHECK_LAUNCH_COLUMN
        ] == CommonConstants.CHECK_LAUNCH_DISABLE:
            status_message = "Check flag is present with 'N' value.Proceeding with existing " \
                             "cluster Id in WAITING state"
            logger.info(status_message)
            if not cluster_ids:
                status_message = "There are no clusters with process id " + str(
                    process_id) + " in WAITING state. Trying to launch new cluster"
                logger.info(status_message)
                emrClusterLaunch.main(process_id, "start")
                # launch_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                # "EmrClusterLaunchWrapper.py") + " -p " + str(process_id) + " -a start"
                # execute_shell_command(launch_string)
    cluster_id = get_cluster_id(process_id)
    kwargs["ti"].xcom_push(key='emr_status', value=CommonConstants.STATUS_SUCCEEDED)
    kwargs["ti"].xcom_push(key='cluster_id', value=str(cluster_id))


def get_cycle_details(process_id, cycle_assigned_flag=False):
    """This method gets cycle details"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    result = {"frequency": "Not Configured",
              "data_date": "Not Configured",
              "cycle_id": "Not Configured"}
    try:
        logger.info("Started preparing query to fetch frequency for process id:" + str(process_id))
        query = "Select frequency from " + audit_db + "." + \
                CommonConstants.EMR_CLUSTER_CONFIGURATION_TABLE + \
                " where process_id=" + str(process_id) + " and active_flag='" + \
                CommonConstants.ACTIVE_IND_VALUE + "'"
        logger.info(
            "Completed preparing query to fetch frequency for process id:" + str(process_id))
        frequency = MySQLConnectionManager().execute_query_mysql(query, False)
        logger.info(
            "Completed query execution to fetch frequency for process id:" + str(process_id))
        data_date_flag = False
        if frequency:
            result["frequency"] = frequency[0]['frequency']
            frequency_str = frequency[0]['frequency']
            logger.info("Frequency retrieved : " + str(result["frequency"]))
            if cycle_assigned_flag:
                logger.info("Starting function to fetch latest cycle id")

                fetch_latest_cycle_query = "select * from log_cycle_dtl where cycle_id in (" \
                                           "select max(cycle_id) from log_cycle_dtl where " \
                                           "process_id = {process_id} and frequency = '" \
                                           "{frequency}')" \
                    .format(audit_db=audit_db, cycle_details=CommonConstants.LOG_CYCLE_DTL,
                            process_id=process_id,
                            frequency=frequency_str)
                logger.info("Query String: " + str(fetch_latest_cycle_query))
                cycle_id_result = MySQLConnectionManager().execute_query_mysql(
                    fetch_latest_cycle_query, True)
                cycle_id = cycle_id_result['cycle_id']
                data_date = cycle_id_result['data_date']
                data_date_flag = True
                result["cycle_id"] = str(cycle_id)
                result["data_date"] = str(data_date)
            if not data_date_flag:
                data_date = CommonUtils().fetch_data_date_process(process_id, result["frequency"])
                logger.info("Data date retrieved : " + str(data_date))
                result["data_date"] = str(data_date)
        logger.info("Found cycle details: " + str(result))
    except Exception as exc:
        logger.error(str(traceback.format_exc()))
        logger.info("Error: " + str(exc.message))
        logger.info("Some metadata information for DW email is not configured")
        logger.info("Found cycle details: " + str(result))
    finally:
        # Return - Cycle ID, Frequency, Data date
        return result


def send_dw_email(email_type, process_id, dag_name, **kwargs):
    """This method send dw email"""
    logger.info("Starting function to send Data Warehouse status email")
    logger.info("Retrieving EMR Launch status to check if Execution has started")
    ti = kwargs["ti"]
    emr_status = ti.xcom_pull(key='emr_status', task_ids="launch_cluster", dag_id=dag_name)
    logger.info("EMR Launch Status:" + str(emr_status))
    trigger_notification_utility(str(email_type), process_id, dag_name, emr_status=str(emr_status))
    logger.info("Completing function to send Data Warehouse notification email")


def terminate_emr(email_type=None, process_id=None, dag_name=None, **kwargs):
    """This method terminates emr"""
    ti = kwargs["ti"]
    emr_status = ti.xcom_pull(key='emr_status', task_ids="launch_cluster", dag_id=dag_name)
    logger.info("EMR Launch Status:" + str(emr_status))
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "SELECT * FROM " + audit_db + "." + CommonConstants.EMR_CLUSTER_CONFIGURATION_TABLE + \
            " WHERE process_id = " + str(process_id) + ";"
    logging.info("Executing query - " + query)
    cluster_configs = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Result of query - " + str(cluster_configs))
    termination_on_failure_flag = ast.literal_eval(cluster_configs[0]["property_json"])["termination_on_failure_flag"]
    logger.info("Value of Termination_on_failure_flag - " + termination_on_failure_flag)
    termination_on_success_flag = ast.literal_eval(cluster_configs[0]["property_json"])["termination_on_success_flag"]
    logger.info("Value of Termination_on_success_flag - " + termination_on_success_flag)

    if str(email_type).lower() == "batch_status":
        trigger_notification_utility(str(email_type), process_id, dag_name,
                                     emr_status=str(emr_status))
        if str(emr_status).lower() == CommonConstants.STATUS_FAILED.lower():
            logger.info("EMR Launch failed hence nothing to terminate")
        if str(emr_status).lower() == CommonConstants.STATUS_SUCCEEDED.lower():
            cluster_id = get_cluster_id(process_id)
            save_ganglia_pdf_to_s3_flag = CommonConstants.SAVE_GANGLIA_PDF_TO_S3
            send_ganglia_pdf_as_attachment_flag = CommonConstants.SEND_GANGLIA_PDF_AS_ATTACHMENT
            save_ganglia_csv_to_s3_flag = CommonConstants.SAVE_GANGLIA_CSV_TO_S3
            if save_ganglia_pdf_to_s3_flag == 'Y' or send_ganglia_pdf_as_attachment_flag == 'Y':
                ganglia_report_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                                  "CreateGangliaMetricsPDF.py"
                                                                  ) + " " + cluster_id
                execute_shell_command(ganglia_report_string)
            configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                           CommonConstants.ENVIRONMENT_CONFIG_FILE))

            s3_bucket_name = "s3://" + configuration.get_configuration(
                [CommonConstants.ENVIRONMENT_PARAMS_KEY, "s3_bucket_name"]) + CommonConstants.S3_FOLDER
            if save_ganglia_csv_to_s3_flag == 'Y':
                ganglia_csv_string = "cd " + CommonConstants.AIRFLOW_CODE_PATH + ";" + \
                                     'python3 GangliaMetricFetchUtility.py ' \
                                     '--cluster_id "' + cluster_id + '" ' \
                                                                     '--s3_path ' + s3_bucket_name + ' ' \
                                                                                                     '--cluster_ip ' + get_host(
                    process_id, ["MASTER"]) + ' ' \
                                              '--node_level "N"; ' \
                                              'python3 GangliaMetricFetchUtility.py ' \
                                              '--cluster_id ' + cluster_id + ' ' \
                                                                             '--s3_path "' + s3_bucket_name + '" ' \
                                                                                                              '--cluster_ip ' + get_host(
                    process_id, ["MASTER"]) + ' ' \
                                              '--node_level "Y"; '
                logger.debug("Ganglia csv command: " + ganglia_csv_string)
                execute_shell_command(ganglia_csv_string)
            trigger_notification_utility("dqm", process_id, dag_name, emr_status=str(emr_status))
            logger.info("EMR Launch successful hence need to evaluate process status before "
                        "termination")
            dataset_ids = get_dataset_ids(process_id)
            final_dataset_id = ",".join(map(str, dataset_ids))
            batch_status = fetch_ingestion_email_batch_info(process_id, final_dataset_id)
            logger.info("Batch Status retrieved: " + str(batch_status))
            status = batch_status["status"]
            logger.info("Final Batch Status: " + str(status))
            if status == CommonConstants.STATUS_FAILED:
                if termination_on_failure_flag.upper() == "Y":
                    cluster_id = get_cluster_id(process_id)
                    copy_spark_logs_to_s3(cluster_id)
                    logger.info("Termination on Failure flag is set hence terminating EMR")
                    terminate_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                                 "TerminateEmrHandler.py"
                                                                 ) + " " + cluster_id
                    execute_shell_command(terminate_string)
                else:
                    logger.info("Termination on Failure flag is NOT set hence NOT terminating EMR")
            elif status == CommonConstants.STATUS_SUCCEEDED:
                if termination_on_success_flag.upper() == "Y":
                    cluster_id = get_cluster_id(process_id)
                    copy_spark_logs_to_s3(cluster_id)
                    logger.info("Termination on Success flag is set hence terminating EMR")
                    logger.info("python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH, "TerminateEmrHandler.py") +
                                " " + cluster_id)
                    terminate_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                                 "TerminateEmrHandler.py") + " " + cluster_id
                    execute_shell_command(terminate_string)
                else:
                    logger.info("Termination on Success flag is NOT set hence NOT terminating EMR")
            else:
                logger.info("Batch Status is not logged as FAILED/SUCCEEDED, hence not "
                            "terminating the EMR")

    elif str(email_type).lower() == "cycle_status":
        trigger_notification_utility(str(email_type), process_id, dag_name,
                                     emr_status=str(emr_status))
        if str(emr_status).lower() == CommonConstants.STATUS_FAILED.lower():
            logger.info("EMR Launch failed hence nothing to terminate")
        if str(emr_status).lower() == CommonConstants.STATUS_SUCCEEDED.lower():
            logger.info("EMR Launch successful hence evaluating cycle status")
            cycle_details = get_cycle_details(process_id, cycle_assigned_flag=True)
            cycle_status = get_cycle_step_status(cycle_details["cycle_id"])
            if cycle_status["status"] == CommonConstants.STATUS_FAILED:
                if termination_on_failure_flag.upper() == "Y":
                    logger.info("Termination on Failure flag is set hence terminating EMR")
                    cluster_id = get_cluster_id(process_id)
                    copy_spark_logs_to_s3(cluster_id)
                    terminate_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                                 "TerminateEmrHandler.py"
                                                                 ) + " " + cluster_id
                    execute_shell_command(terminate_string)
                else:
                    logger.info("Termination on Failure flag is NOT set hence NOT terminating EMR")

            elif cycle_status["status"] == CommonConstants.STATUS_SUCCEEDED:
                if termination_on_success_flag.upper() == "Y":
                    logger.info("Termination on Success flag is set hence terminating EMR")

                    cluster_id = get_cluster_id(process_id)
                    copy_spark_logs_to_s3(cluster_id)
                    terminate_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                                 "TerminateEmrHandler.py"
                                                                 ) + " " + cluster_id
                    execute_shell_command(terminate_string)
                else:
                    logger.info("Termination on Success flag is NOT set hence NOT terminating EMR")
            else:
                logger.info("Cycle status is neither SUCCESS not FAILED hence not "
                            "terminating the EMR")
    else:
        logger.info("No Email type given as input hence no email will be sent at this step")
        ti = kwargs["ti"]
        emr_status = ti.xcom_pull(key='emr_status', task_ids="launch_cluster", dag_id=dag_name)
        logger.info("EMR Launch Status:" + str(emr_status))
        if str(emr_status).lower() == CommonConstants.STATUS_FAILED.lower():
            logger.info("EMR Launch failed hence nothing to terminate")
        else:
            logger.info("Terminating EMR")
            cluster_id = get_cluster_id(process_id)
            terminate_string = "python3 " + os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                         "TerminateEmrHandler.py") + " " + cluster_id
            execute_shell_command(terminate_string)


def copy_spark_logs_to_s3(cluster_id=None):
    """This method copies spark logs to s3"""
    try:
        status_message = "Starting to execute copy_spark_logs_to_s3 for cluster id: " + \
                         str(cluster_id)
        logging.info(status_message)

        if cluster_id is None:
            raise Exception("Cluster id can not None")

        configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                       CommonConstants.ENVIRONMENT_CONFIG_FILE))

        spark_logs_s3_path = configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, "spark_logs_s3_path"])
        # private_key_location = configuration.get_configuration(
        #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])

        copy_to_s3_flag = True
        if spark_logs_s3_path is None:
            copy_to_s3_flag = False
        if spark_logs_s3_path is "":
            copy_to_s3_flag = False
        if spark_logs_s3_path == "":
            copy_to_s3_flag = False

        logging.info("spark_logs_s3_path is : " + str(spark_logs_s3_path))

        logging.info("copy_to_s3_flag is : " + str(copy_to_s3_flag))

        if copy_to_s3_flag:
            logs_s3_path = str(spark_logs_s3_path).rstrip("/") + "/"
            master_ip = CommonUtils().get_master_ip_from_cluster_id(cluster_id)
            default_spark_logs_location = CommonConstants.SPARK_LOGS_DEFAULT_PATH_EMR

            if os.path.isdir(default_spark_logs_location):
                command_to_execute = "cd " + CommonConstants.AIRFLOW_CODE_PATH + \
                                     "; python3 ClusterToS3LoadHandler.py  -s " + \
                                     default_spark_logs_location + " -t " + logs_s3_path
                status_message = "Command created for log transfer: " + str(command_to_execute)
                logging.info(status_message)
                # ssh = ExecuteSSHCommand(remote_host=master_ip, username=CommonConstants.EMR_USER_NAME,
                # key_file=private_key_location)
                output = SystemManagerUtility().execute_command(cluster_id, command_to_execute)
                logging.info("Output generated by job executor " + str(output))
            else:
                logging.info("No logs generated by spark apps. Nothing to backup")

        status_message = "Completing function execute copy_spark_logs_to_s3"
        logging.info(status_message)

    except Exception as exception:
        status_message = "Exception in copy_spark_logs_to_s3 due to  " + str(exception)
        logger.error(str(traceback.format_exc()))
        logger.error(status_message)
        raise exception


def call_pre_landing(file_master_id, dag_name, process_id, **kwargs):
    """This method calls pre-landing"""
    kwargs["ti"].xcom_push(key='emr_status', value=CommonConstants.STATUS_SUCCEEDED)
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])

    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    context = kwargs
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_file_path = str('/tmp/batches/').__add__(
        dt.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')).__add__('_').__add__(
        str(file_master_id)).__add__('.txt')
    max_try_limit = 3
    count = 0
    if conf_value.find('--deploy-mode cluster') != -1:
        logger.info("================Executing job in Spark cluster mode================")
        ##Adding mandatory max attempt as 1
        conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
        conf_value = conf_value + " --name PreLanding_" + file_master_id
        logger.info(
            "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh download.sh " + " " + str(file_master_id) + " " + str(
                cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + batch_file_path + " " + '"' + str(
                conf_value) + '"')
        try:
            output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                             "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh download.sh " + " " + str(
                                                                                 file_master_id) + " " + str(
                                                                                     cluster_id) + " " + str(
                                                                                     dag_id) + " " + str(
                                                                                     process_id) + " " + batch_file_path + " " + '"' + str(
                                                                                     conf_value) + '"')
            while True:
                try:
                    batches = fetch_batch_id_prelanding(cluster_id, process_id, file_master_id, dag_id)
                    logging.info("Output generated by pre landing task - " + str(output))
                    kwargs["ti"].xcom_push(key='batch_ids', value=batches)
                except:
                    if count != max_try_limit:
                        count = count + 1
                        logging.info("No batches has been returned so retrying again in 5 seconds for %s ATTEMPT",
                                     count)
                        time.sleep(5)
                        continue
                    else:
                        logging.info("Max attempts have been reached.")
                        time.sleep(2)
                        logging.info("Unexpected error: %s ", sys.exc_info()[0])
                        raise
                break
        except Exception as exception:
            status_message = "Spark Submit failed due to" + str(exception)
            logger.error(status_message)
        finally:
            response = CommonUtils().get_yarn_app_id_status(app_name="PreLanding_" + file_master_id, cluster_ip=ip)
            yarn_app_id = response["app_id"]
            yarn_app_state = response["state"]
            yarn_diagnostics_info = response["diagnostic_message"]
            logger.error("Diagnostics Info:" + yarn_diagnostics_info)
            output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
            logger.info("This RUN was configured in cluster mode, To get full logs run the "
                        "following command on EMR {yarn_log_command}".format(
                yarn_log_command=output_cmnd
            ))
            # logging.info("Output generated by task - " + str(output1))

            if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                status_message = "Spark Job Failed"
                raise Exception(status_message)

    else:
        logger.info("================Executing job in Spark client mode================")
        logger.info(
            "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh download.sh " + " " + str(file_master_id) + " " + str(
                cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + batch_file_path + " " + '"' + str(
                conf_value) + '"')
        output = SystemManagerUtility().execute_command(cluster_id,
                                                        "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh download.sh " + " " + str(
                                                            file_master_id) + " " + str(
                                                            cluster_id) + " " + str(dag_id) + " " + str(
                                                            process_id) + " " + batch_file_path + " " + '"' + str(
                                                            conf_value) + '"')

        while True:
            try:
                batches = fetch_batch_id_prelanding(cluster_id, process_id, file_master_id, dag_id)
                logging.info("Output generated by pre landing task - " + str(output))
                kwargs["ti"].xcom_push(key='batch_ids', value=batches)
            except:
                if count != max_try_limit:
                    count = count + 1
                    logging.info("No batches has been returned so retrying again in 5 seconds for %s ATTEMPT", count)
                    time.sleep(5)
                    continue
                else:
                    logging.info("Max attempts have been reached.")
                    time.sleep(2)
                    logging.info("Unexpected error: %s ", sys.exc_info()[0])
                    raise
            break


def call_file_check(file_master_id, dag_name, process_id, **kwargs):
    """This method calls file check"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    ti = kwargs["ti"]
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break
        if conf_value.find('--deploy-mode cluster') != -1:
            logger.info("================Executing job in Spark cluster mode================")
            ##Adding mandatory max attempt as 1
            conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
            conf_value = conf_value + " --name FileCheck_" + file_master_id + "_" + batch_id
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh file_check.sh " +
                        str(file_master_id) + " " + str(cluster_id) + " " + str(dag_id) + " " +
                        str(process_id) + " " + str(batch_id) + " " + '"' + str(conf_value) + '"' + " " + str(batches))
            try:
                output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                 "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh file_check.sh " +
                                                                                 str(file_master_id) + " " + str(
                                                                                     cluster_id) + " " + str(
                                                                                     dag_id) + " " +
                                                                                 str(process_id) + " " + str(
                                                                                     batch_id) + " " + '"' + str(
                                                                                     conf_value) + '"' +
                                                                                 " " + str(batches))
                logging.info("Output generated by file check task - " + str(output))
            except Exception as exception:
                status_message = "Spark Submit failed due to" + str(exception)
                logger.error(status_message)
            finally:
                response = CommonUtils().get_yarn_app_id_status(app_name="FileCheck_" + file_master_id, cluster_ip=ip)
                yarn_app_id = response["app_id"]
                yarn_app_state = response["state"]
                yarn_diagnostics_info = response["diagnostic_message"]
                logger.error("Diagnostics Info:" + yarn_diagnostics_info)

                output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)

                logger.info("This RUN was configured in cluster mode, To get full logs run the "
                            "following command on EMR {yarn_log_command}".format(
                    yarn_log_command=output_cmnd
                ))

                CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_FILE_CHECK,
                                                               batch_id=batch_id)

                if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                    status_message = "Spark Job Failed"
                    raise Exception(status_message)
        else:
            logger.info("================Executing job in Spark client mode================")
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh file_check.sh " +
                        str(file_master_id) + " " + str(cluster_id) + " " + str(dag_id) + " " +
                        str(process_id) + " " + str(batch_id) + " " + '"' + str(conf_value) + '"' + " " + str(batches))
            output = SystemManagerUtility().execute_command(cluster_id,
                                                            "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh file_check.sh " +
                                                            str(file_master_id) + " " + str(cluster_id) + " " + str(
                                                                dag_id) + " " +
                                                            str(process_id) + " " + str(batch_id) + " " + '"' + str(
                                                                conf_value) + '"' +
                                                            " " + str(batches))
            logging.info("Output generated by file check task - " + str(output))


def call_landing(file_master_id, dag_name, process_id, **kwargs):
    """This method calls landing"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    conf_value = get_spark_configurations(file_master_id, process_id)
    if len(conf_value) != 0:
        conf_value = conf_value.replace(
            '--master yarn --deploy-mode cluster --files $(cat /home/hadoop/temp_file) --py-files $(cat /home/hadoop/temp_py)',
            '')
        conf_value = conf_value.strip()
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    ti = kwargs["ti"]
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break
        logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh landing.sh " + str(file_master_id) + " " +
                    str(cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + str(batch_id) + " " +
                    str(batches) + ' "' + str(conf_value) + '"')
        output = SystemManagerUtility().execute_command(cluster_id,
                                                        "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh landing.sh " + str(
                                                            file_master_id) +
                                                        " " + str(cluster_id) + " " + str(dag_id) + " " + str(
                                                            process_id) + " " +
                                                        str(batch_id) + " " + str(batches) + ' "' + str(
                                                            conf_value) + '"')
        logging.info("Output generated by landing task - " + str(output))


def call_pre_dqm(file_master_id, dag_name, process_id, **kwargs):
    """This method calls pre dqm"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    ti = kwargs["ti"]
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break

        if conf_value.find('--deploy-mode cluster') != -1:
            logger.info("================Executing job in Spark cluster mode================")
            ##Adding mandatory max attempt as 1
            conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
            conf_value = conf_value + " --name PreDQM_" + file_master_id + "_" + batch_id
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh pre_dqm.sh " + str(file_master_id) + " " +
                        str(cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + str(batch_id) +
                        " " + '"' + str(conf_value) + '"' + " " + str(batches))
            try:
                output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                 "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh pre_dqm.sh " +
                                                                                 str(file_master_id) + " " + str(
                                                                                     cluster_id) + " " + str(
                                                                                     dag_id) + " " +
                                                                                 str(process_id) + " " + str(batch_id) +
                                                                                 " " + '"' + str(
                                                                                     conf_value) + '"' + " " + str(
                                                                                     batches))
                logging.info("Output generated by pre_dqm task - " + str(output))
            except Exception as exception:
                status_message = "Spark Submit failed due to" + str(exception)
                logger.error(status_message)
            finally:
                response = CommonUtils().get_yarn_app_id_status(app_name="PreDQM_" + file_master_id, cluster_ip=ip)
                yarn_app_id = response["app_id"]
                yarn_app_state = response["state"]
                yarn_diagnostics_info = response["diagnostic_message"]
                logger.error("Diagnostics Info:" + yarn_diagnostics_info)

                output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                logger.info("This RUN was configured in cluster mode, To get full logs run the "
                            "following command on EMR {yarn_log_command}".format(
                    yarn_log_command=output_cmnd
                ))

                CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_PRE_DQM,
                                                               batch_id=batch_id)

                if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                    status_message = "Spark Job Failed"
                    raise Exception(status_message)
        else:
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh pre_dqm.sh " + str(file_master_id) + " " +
                        str(cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + str(batch_id) +
                        " " + '"' + str(conf_value) + '"' + " " + str(batches))
            output = SystemManagerUtility().execute_command(cluster_id,
                                                            "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh pre_dqm.sh " +
                                                            str(file_master_id) + " " + str(cluster_id) + " " + str(
                                                                dag_id) + " " +
                                                            str(process_id) + " " + str(batch_id) +
                                                            " " + '"' + str(conf_value) + '"' + " " + str(batches))
            logging.info("Output generated by pre_dqm task - " + str(output))


def call_dqm(file_master_id, dag_name, process_id, **kwargs):
    """This method calls dqm"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    # [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    ti = kwargs["ti"]
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break

        if conf_value.find('--deploy-mode cluster') != -1:
            logger.info("================Executing job in Spark cluster mode================")
            ##Adding mandatory max attempt as 1
            conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
            conf_value = conf_value + " --name DQMCheckHandler_" + file_master_id + "_" + batch_id
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh dqm.sh "
                        + str(file_master_id) + " " + str(cluster_id) + " " +
                        str(dag_id) + " " + str(process_id) + " " + str(batch_id) +
                        " " + '"' + str(conf_value) + '"' + " " + str(batches))
            try:
                output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                 "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh dqm.sh "
                                                                                 + str(file_master_id) + " " + str(
                                                                                     cluster_id) + " " +
                                                                                 str(dag_id) + " " + str(
                                                                                     process_id) + " " + str(batch_id) +
                                                                                 " " + '"' + str(
                                                                                     conf_value) + '"' + " " + str(
                                                                                     batches))
                logging.info("Output generated by dqm task - " + str(output))
            except Exception as exception:
                status_message = "Spark Submit failed due to" + str(exception)
                logger.error(status_message)
            finally:
                response = CommonUtils().get_yarn_app_id_status(app_name="DQMCheckHandler_" + file_master_id,
                                                                cluster_ip=ip)
                yarn_app_id = response["app_id"]
                yarn_app_state = response["state"]
                yarn_diagnostics_info = response["diagnostic_message"]
                logger.error("Diagnostics Info:" + yarn_diagnostics_info)
                output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                logger.info("This RUN was configured in cluster mode, To get full logs run the "
                            "following command on EMR {yarn_log_command}".format(
                    yarn_log_command=output_cmnd
                ))

                CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_DQM,
                                                               batch_id=batch_id)
                if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                    status_message = "Spark Job Failed"
                    raise Exception(status_message)

        else:
            logger.info("================Executing job in Spark client mode================")
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh dqm.sh "
                        + str(file_master_id) + " " + str(cluster_id) + " " +
                        str(dag_id) + " " + str(process_id) + " " + str(batch_id) +
                        " " + '"' + str(conf_value) + '"' + " " + str(batches))
            output = SystemManagerUtility().execute_command(cluster_id,
                                                            "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh dqm.sh "
                                                            + str(file_master_id) + " " + str(cluster_id) + " " +
                                                            str(dag_id) + " " + str(process_id) + " " + str(batch_id) +
                                                            " " + '"' + str(conf_value) + '"' + " " + str(batches))
            logging.info("Output generated by dqm task - " + str(output))


def call_dqm_filter(file_master_id, dag_name, process_id, **kwargs):
    """This method calls dqm filter"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    # [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    ti = kwargs["ti"]
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break

        if conf_value.find('--deploy-mode cluster') != -1:
            logger.info("================Executing job in Spark cluster mode================")
            ##Adding mandatory max attempt as 1
            conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
            conf_value = conf_value + " --name DQMFilter_" + file_master_id + "_" + batch_id
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh dqm_filter.sh " + str(file_master_id) + " " +
                        str(cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + str(batch_id) + " " + '"' +
                        str(conf_value) + '"' + " " + str(batches))
            try:
                output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                 "cd " + CommonConstants.AIRFLOW_CODE_PATH +
                                                                                 "; sh dqm_filter.sh " + str(
                                                                                     file_master_id) + " " +
                                                                                 str(cluster_id) + " " + str(
                                                                                     dag_id) + " " + str(process_id) +
                                                                                 " " + str(batch_id) + " " + '"' + str(
                                                                                     conf_value) + '"' +
                                                                                 " " + str(batches))
                logging.info("Output generated by dqm_filter task - " + str(output))
            except Exception as exception:
                status_message = "Spark Submit failed due to" + str(exception)
                logger.error(status_message)
            finally:
                response = CommonUtils().get_yarn_app_id_status(app_name="DQMFilter_" + file_master_id, cluster_ip=ip)
                yarn_app_id = response["app_id"]
                yarn_app_state = response["state"]
                yarn_diagnostics_info = response["diagnostic_message"]
                logger.error("Diagnostics Info:" + yarn_diagnostics_info)
                output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                # output1 = SystemManagerUtility().execute_command(cluster_id,output_cmnd)
                # logging.info("Output generated by task - " + str(output1))
                logger.info("This RUN was configured in cluster mode, To get full logs run the "
                            "following command on EMR {yarn_log_command}".format(
                    yarn_log_command=output_cmnd
                ))

                CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_STAGING,
                                                               batch_id=batch_id)
                if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                    status_message = "Spark Job Failed"
                    raise Exception(status_message)
        else:
            logger.info("================Executing job in Spark client mode================")
            logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh dqm_filter.sh " + str(file_master_id) + " " +
                        str(cluster_id) + " " + str(dag_id) + " " + str(process_id) + " " + str(batch_id) + " " + '"' +
                        str(conf_value) + '"' + " " + str(batches))
            output = SystemManagerUtility().execute_command(cluster_id, "cd " + CommonConstants.AIRFLOW_CODE_PATH +
                                                            "; sh dqm_filter.sh " + str(file_master_id) + " " +
                                                            str(cluster_id) + " " + str(dag_id) + " " + str(
                process_id) +
                                                            " " + str(batch_id) + " " + '"' + str(conf_value) + '"' +
                                                            " " + str(batches))
            logging.info("Output generated by dqm_filter task - " + str(output))


def copyhdfstos3(file_master_id, dag_name, process_id, **kwargs):
    """This method copies from hdfs to s3"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    ti = kwargs["ti"]
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        if conf_value.find('--deploy-mode cluster') != -1:
            logger.info("================Executing job in Spark cluster mode================")
            ##Adding mandatory max attempt as 1
            conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
            conf_value = conf_value + " --name copyhdfstos3_" + file_master_id + "_" + batch_id
            logging.info("Output generated by copy_to_s3 task - " + str(output))
            try:
                output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                 "cd " + CommonConstants.AIRFLOW_CODE_PATH +
                                                                                 "; sh copyhdfstos3.sh " + str(
                                                                                     file_master_id) + " " +
                                                                                 str(cluster_id) + " " + str(
                                                                                     dag_id) + " " + str(process_id) +
                                                                                 " " + str(batch_id) + " " + '"' + str(
                                                                                     conf_value) + '"' + " " +
                                                                                 str(batches))
                logging.info("Output generated by copy_to_s3 task - " + str(output))
            except Exception as exception:
                status_message = "Spark Submit failed due to" + str(exception)
                logger.error(status_message)
            finally:
                response = CommonUtils().get_yarn_app_id_status(app_name="copyhdfstos3_" + file_master_id,
                                                                cluster_ip=ip)
                yarn_app_id = response["app_id"]
                yarn_app_state = response["state"]
                yarn_diagnostics_info = response["diagnostic_message"]
                logger.error("Diagnostics Info:" + yarn_diagnostics_info)
                output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                logger.info("This RUN was configured in cluster mode, To get full logs run the "
                            "following command on EMR {yarn_log_command}".format(
                    yarn_log_command=output_cmnd
                ))

                CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_HDFS_TO_S3,
                                                               batch_id=batch_id)
                if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                    status_message = "Spark Job Failed"
                    raise Exception(status_message)
        else:
            logger.info("================Executing job in Spark client mode================")
            output = SystemManagerUtility().execute_command(cluster_id, "cd " + CommonConstants.AIRFLOW_CODE_PATH +
                                                            "; sh copyhdfstos3.sh " + str(file_master_id) + " " +
                                                            str(cluster_id) + " " + str(dag_id) + " " + str(
                process_id) +
                                                            " " + str(batch_id) + " " + '"' + str(
                conf_value) + '"' + " " +
                                                            str(batches))
            logging.info("Output generated by copy_to_s3 task - " + str(output))


def archive(file_master_id, dag_name, process_id, **kwargs):
    """This method archives data"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ti = kwargs["ti"]
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break

        if conf_value.find('--deploy-mode cluster') != -1:
            logger.info("================Executing job in Spark cluster mode================")
            ##Adding mandatory max attempt as 1
            conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
            conf_value = conf_value + " --name Archive_" + file_master_id + "_" + batch_id
            try:
                output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                 "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh archive.sh "
                                                                                 + str(file_master_id) + " " + str(
                                                                                     cluster_id) + " " +
                                                                                 str(dag_id) + " " + str(
                                                                                     process_id) + " " + str(batch_id) +
                                                                                 " " + '"' + str(
                                                                                     conf_value) + '"' + " " + str(
                                                                                     batches))
                logging.info("Output generated by archive task - " + str(output))
            except Exception as exception:
                status_message = "Spark Submit failed due to" + str(exception)
                logger.error(status_message)
            finally:
                response = CommonUtils().get_yarn_app_id_status(app_name="Archive_" + file_master_id, cluster_ip=ip)
                yarn_app_id = response["app_id"]
                yarn_app_state = response["state"]
                yarn_diagnostics_info = response["diagnostic_message"]
                logger.error("Diagnostics Info:" + yarn_diagnostics_info)
                output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                logger.info("This RUN was configured in cluster mode, To get full logs run the "
                            "following command on EMR {yarn_log_command}".format(
                    yarn_log_command=output_cmnd
                ))

                CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_ARCHIVE,
                                                               batch_id=batch_id)
                if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                    status_message = "Spark Job Failed"
                    raise Exception(status_message)
        else:
            logger.info("================Executing job in Spark client mode================")
            output = SystemManagerUtility().execute_command(cluster_id,
                                                            "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh archive.sh "
                                                            + str(file_master_id) + " " + str(cluster_id) + " " +
                                                            str(dag_id) + " " + str(process_id) + " " + str(batch_id) +
                                                            " " + '"' + str(conf_value) + '"' + " " + str(batches))
            logging.info("Output generated by archive task - " + str(output))


def call_staging(file_master_id, dag_name, process_id, **kwargs):
    """This method calls staging"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    ti = kwargs["ti"]
    ip = get_host(process_id, ["MASTER"])
    cluster_id = get_cluster_id(process_id)
    logging.info("IP---------- - " + str(ip))
    batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                           dag_id=dag_name)
    logging.info("Batch Id fetched from pre landing is - " + str(batches))
    context = kwargs
    # batches=batches.decode('utf-8')
    conf_value = get_spark_configurations(file_master_id, process_id)
    dag_id = str(context['dag_run'].run_id)
    # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
    batch_ids = batches.split(",")
    for batch_id in batch_ids:
        processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id, process_id)
        if processed_flag:
            break
        logger.info("cd " + CommonConstants.AIRFLOW_CODE_PATH + "; python3 AuditHandler.py "
                    + str(file_master_id) + " " + str(cluster_id) + " " + str(dag_id) + " " +
                    " " + str(process_id) + " " + str(batch_id) + " " + str(batch_ids))
        output = SystemManagerUtility().execute_command(cluster_id,
                                                        "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; python3 AuditHandler.py "
                                                        + str(file_master_id) + " " + str(cluster_id) + " " + str(
                                                            dag_id) + " " +
                                                        " " + str(process_id) + " " + str(batch_id) + " " + str(
                                                            batch_ids))
        logging.info("Output generated by audit update  task - " + str(output))


def publish(file_master_id, dag_name, process_id, **kwargs):
    """This method publishes data"""
    s3_backup_flag = False
    try:
        configuration = JsonConfigUtility(
            os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
        # private_key_location = configuration.get_configuration(
        #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
        ti = kwargs["ti"]
        ip = get_host(process_id, ["MASTER"])
        cluster_id = get_cluster_id(process_id)
        batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                               dag_id=dag_name)
        logging.info("Batch Id fetched from pre landing is - " + str(batches))
        context = kwargs
        # batches=batches.decode('utf-8')
        conf_value = get_spark_configurations(file_master_id, process_id)
        dag_id = str(context['dag_run'].run_id)
        # Adding logic to clear s3 path in case of latest load
        logger.info("Checking load type for the dataset_id " + str(file_master_id))
        audit_db = configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
        get_load_type_query = "SELECT publish_s3_path,publish_type from" \
                              " {audit_db}.{process_date_table}" \
                              " where dataset_id='{dataset_id}' ".format \
            (audit_db=audit_db,
             process_date_table=CommonConstants.DATASOURCE_INFORMATION_TABLE_NAME,
             dataset_id=file_master_id)
        # Get publish_s3_path and publish_type for the dataset
        total_batch_ids = batches.split(",")
        skip_batch_ids = []
        batch_ids = []
        skip_flag = 'N'
        query_output = MySQLConnectionManager().execute_query_mysql(get_load_type_query)
        logging.info("Query output" + str(query_output))
        if len(query_output) == 1 and "publish_type" in query_output[0]:
            for batch_id in total_batch_ids:
                processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id,
                                                                              process_id)
                if processed_flag:
                    skip_flag = 'Y'
                    break
            if query_output[0]["publish_type"].lower() == CommonConstants.LATEST_LOAD_TYPE.lower() and skip_flag == 'N':
                batch_ids = sorted(total_batch_ids, reverse=True)[:1]
                skip_batch_ids = sorted(total_batch_ids, reverse=True)[1:]
                client = boto3.client('s3')
                parsed_url = urlparse(query_output[0]["publish_s3_path"])
                # Check if the Publish s3 path contains any data, backup data if exists
                file_list = client.list_objects(Bucket=parsed_url.netloc,
                                                Prefix=os.path.join(parsed_url.path, "").lstrip("/"))
                if "Contents" in file_list and len(file_list["Contents"]) > 0:
                    s3_backup_flag = True
                    CustomS3Utility().move_s3_to_s3_recursively(os.path.join(query_output[0]["publish_s3_path"], ""),
                                                                os.path.join(query_output[0]["publish_s3_path"].rstrip(
                                                                    "/") + "_backup", ""))
                    logger.info("Backup of publish data is complete and s3_backup_flag is set as True")
                else:
                    logger.info("No file is found in " + query_output[0]["publish_s3_path"])
        # ssh = ExecuteSSHCommand(remote_host=ip, username="hadoop", key_file=private_key_location)
        batch_ids = batches.split(",")
        for batch_id in batch_ids:
            processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id,
                                                                          process_id)
            if processed_flag:
                break

            if conf_value.find('--deploy-mode cluster') != -1:
                logger.info("================Executing job in Spark cluster mode================")
                ##Adding mandatory max attempt as 1
                conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
                conf_value = conf_value + " --name Publish_" + file_master_id + "_" + batch_id
                try:
                    output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                     "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh publish.sh "
                                                                                     + str(file_master_id) + " " + str(
                                                                                         cluster_id) + " " +
                                                                                     str(dag_id) + " " + str(
                                                                                         process_id) + " " + str(
                                                                                         batch_id) +
                                                                                     " " + '"' + str(
                                                                                         conf_value) + '"' + " " + str(
                                                                                         batches))
                    logging.info("Output generated by publish task - " + str(output))
                except Exception as exception:
                    status_message = "Spark Submit failed due to" + str(exception)
                    logger.error(status_message)
                finally:
                    # yarn_app_cmnd = "yarn application -list -appStates FINISHED FAILED KILLED | grep Publish_" + file_master_id + "| awk '{print $1;}' | sort -r"
                    # yarn_app_id = SystemManagerUtility().execute_command(cluster_id,yarn_app_cmnd)
                    # yarn_app_id = yarn_app_id.decode('utf-8')
                    # yarn_app_id = yarn_app_id.split('\n')
                    # yarn_app_id = yarn_app_id[0]
                    # yarn_app_state_cmnd = "yarn application -list -appStates FINISHED FAILED KILLED | grep " + yarn_app_id + " | awk '{print $7;}'"
                    # yarn_app_state = SystemManagerUtility().execute_command(cluster_id,yarn_app_state_cmnd)
                    # yarn_app_state = yarn_app_state.decode('utf-8')
                    # yarn_app_state = yarn_app_state.split('\n')
                    # yarn_app_state = yarn_app_state[0]
                    response = CommonUtils().get_yarn_app_id_status(app_name="Publish_" + file_master_id,
                                                                    cluster_ip=ip)
                    yarn_app_id = response["app_id"]
                    yarn_app_state = response["state"]
                    yarn_diagnostics_info = response["diagnostic_message"]
                    logger.error("Diagnostics Info:" + yarn_diagnostics_info)
                    output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                    # output1 = SystemManagerUtility().execute_command(cluster_id,output_cmnd)
                    # logging.info("Output generated by task - " + str(output1))
                    logger.info("This RUN was configured in cluster mode, To get full logs run the "
                                "following command on EMR {yarn_log_command}".format(
                        yarn_log_command=output_cmnd
                    ))

                    CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_PUBLISH,
                                                                   batch_id=batch_id)

                    if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                        status_message = "Spark Job Failed"
                        raise Exception(status_message)
            else:
                output = SystemManagerUtility().execute_command(cluster_id,
                                                                "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; sh publish.sh "
                                                                + str(file_master_id) + " " + str(cluster_id) + " " +
                                                                str(dag_id) + " " + str(process_id) + " " + str(
                                                                    batch_id) +
                                                                " " + '"' + str(conf_value) + '"' + " " + str(batches))
                logging.info("Output generated by publish task - " + str(output))

        # for skip_batch_id in skip_batch_ids:
        #     PROCESS_NAME = "Publish"
        #     curr_batch_id = skip_batch_id
        #     file_details = CommonUtils().get_file_details_for_current_batch(curr_batch_id, str(file_master_id))
        #     for file_detail in file_details:
        #         # Get detail for each file
        #         # Logic added for restartability
        #         file_id = file_detail['file_id']
        #         file_name = file_detail['file_name']
        #         if not file_id or not file_name:
        #             raise Exception(CommonConstants.INVALID_FILE_AUDIT_DETAILS)
        #         log_info = {CommonConstants.BATCH_ID: curr_batch_id, CommonConstants.FILE_ID: file_id,
        #                     CommonConstants.PROCESS_NAME: PROCESS_NAME,
        #                     CommonConstants.PROCESS_STATUS: CommonConstants.STATUS_RUNNING,
        #                     CommonConstants.FILE_NAME: file_name,
        #                     CommonConstants.DATASET_ID: str(file_master_id),
        #                     CommonConstants.CLUSTER_ID: str(cluster_id),
        #                     CommonConstants.WORKFLOW_ID: str(dag_id),
        #                     'process_id': str(process_id)
        #                     }
        #         configuration = JsonConfigUtility(
        #             os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
        #         Mysql_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
        #         response = CommonUtils().create_skip_process_log_entry(Mysql_db, log_info)
        #         if response:
        #             logger.info("Skip Record Inserted Successfully in publish for ", str(skip_batch_id))
        #         else:
        #             logger.info("Skip Record failed to insert Successfully in publish for ", str(skip_batch_id))

    except Exception as exc:
        if s3_backup_flag is True:
            logger.info("s3_backup_flag is set as True, moving the backup data")
            client = boto3.client('s3')
            parsed_url = urlparse(query_output[0]["publish_s3_path"])
            # Check if the Publish s3 path contains any data, backup data if exists
            file_list = client.list_objects(Bucket=parsed_url.netloc,
                                            Prefix=os.path.join(parsed_url.path, "").lstrip("/"))
            if "Contents" in file_list and len(file_list["Contents"]) > 0:
                logger.info("Deleting half copied data in publish")
                CustomS3Utility().delete_s3_objects_recursively(os.path.join(query_output[0]["publish_s3_path"], ""))
            else:
                logger.info("No file is found in " + query_output[0]["publish_s3_path"])
            logger.info("Moving backup data to publish")
            CustomS3Utility().move_s3_to_s3_recursively(
                s3_source_location=os.path.join(query_output[0]["publish_s3_path"].rstrip("/") + "_backup", ""),
                s3_target_location=os.path.join(query_output[0]["publish_s3_path"], ""))

        else:
            logger.info("s3_backup_flag is set as False no action will be performed")
        raise exc
    finally:
        if s3_backup_flag is True:
            logger.info("In finally block and s3_backup_flag is True")
            client = boto3.client('s3')
            parsed_url = urlparse(query_output[0]["publish_s3_path"].rstrip("/") + "_backup")
            # Check if the Publish s3 path contains any data, backup data if exists
            file_list = client.list_objects(Bucket=parsed_url.netloc,
                                            Prefix=os.path.join(parsed_url.path, "").lstrip("/"))

            if "Contents" in file_list and len(file_list["Contents"]) > 0:
                CustomS3Utility().delete_s3_objects_recursively(
                    os.path.join(query_output[0]["publish_s3_path"].rstrip("/") + "_backup", ""))
            else:
                logger.info("No file is found in " + query_output[0]["publish_s3_path"])


def call_job_executor(process_id, frequency, step_name, **kwargs):
    """This method calls job executor"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    context = kwargs
    dag_id = str(context['dag_run'].run_id)
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    if CommonConstants.CLUSTER_MASTER_SUBMIT_FLAG == CommonConstants.ACTIVE_IND_VALUE:
        ip = get_host(process_id, ["MASTER"])
    else:
        ip = get_host(process_id, ["MASTER", "CORE"])
    cluster_id = get_cluster_id(str(process_id))
    data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
    cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
    logging.info("IP---------- - " + str(ip))
    # ssh = ExecuteSSHCommand(remote_host=ip, username=CommonConstants.EMR_USER_NAME,
    # key_file=private_key_location)
    command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; python3 JobExecutor.py" + " " + \
              str(process_id) + " " + str(frequency) + " " + str(step_name) + " " + \
              str(data_date) + " " + str(cycle_id) + " " + str(dag_id)
    logging.info("Command to be executed:" + command)
    output = SystemManagerUtility().execute_command(cluster_id, command)
    logging.info("Output generated by job executor:" + str(output))


def populate_dependency_details(process_id, frequency, **kwargs):
    """This method populates dependency details"""
    logging.info("Started Populate dependency list process for process_id:" +
                 str(process_id) + " " + "and frequency:" + str(frequency))
    context = kwargs
    dag_id = str(context['dag_run'].run_id)
    populate_dependency_handler = PopulateDependency(process_id, frequency, dag_id)
    populate_dependency_handler.populate_dependency_list()
    logging.info("Completed Populate dependency list process for process_id:" +
                 str(process_id) + " " + "and frequency:" + str(frequency))


def copy_data_s3_hdfs(process_id, frequency, **kwargs):
    """This method copies data from s3 to hdfs"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    cluster_id = get_cluster_id(str(process_id))
    if CommonConstants.CLUSTER_MASTER_SUBMIT_FLAG == CommonConstants.ACTIVE_IND_VALUE:
        ip = get_host(process_id, ["MASTER"])
    else:
        ip = get_host(process_id, ["MASTER", "CORE"])
    context = kwargs
    dag_id = str(context['dag_run'].run_id)
    data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
    cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
    logging.info("IP---------- - " + str(ip))
    # ssh = ExecuteSSHCommand(remote_host=ip, username=CommonConstants.EMR_USER_NAME,
    # key_file=private_key_location)
    command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; python3 S3ToHdfsCopyDWHandler.py" + \
              " " + str(process_id) + \
              " " + str(frequency) + " " + str(data_date) + " " + str(cycle_id) + " " + str(dag_id)
    logging.info("Command to be executed:" + command)
    output = SystemManagerUtility().execute_command(cluster_id, command)
    logging.info("Output generated by S3toHdfsCopyDWHandler:" + str(output))


def launch_ddl_creation(process_id, frequency, **kwargs):
    """This method launches ddl creation"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    cluster_id = get_cluster_id(str(process_id))
    if CommonConstants.CLUSTER_MASTER_SUBMIT_FLAG == CommonConstants.ACTIVE_IND_VALUE:
        ip = get_host(process_id, ["MASTER"])
    else:
        ip = get_host(process_id, ["MASTER", "CORE"])
    context = kwargs
    dag_id = str(context['dag_run'].run_id)
    data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
    cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
    logging.info("IP---------- - " + str(ip))
    # ssh = ExecuteSSHCommand(remote_host=ip, username=CommonConstants.EMR_USER_NAME,
    # key_file=private_key_location)
    command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; /usr/lib/spark/bin/spark-submit LaunchDDLCreationHandler.py" + \
              " " + str(process_id) + " " + str(frequency) + " " + str(data_date) + \
              " " + str(cycle_id) + " " + str(dag_id)
    logging.info("Command to be executed:" + command)
    output = SystemManagerUtility().execute_command(cluster_id, command)
    logging.info("Output generated by LaunchDDLHandler :" + str(output))


def publish_step_dw(process_id, frequency, **kwargs):
    """This method publishes dw step"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    # private_key_location = configuration.get_configuration(
    #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, "private_key_loaction"])
    if CommonConstants.CLUSTER_MASTER_SUBMIT_FLAG == CommonConstants.ACTIVE_IND_VALUE:
        ip = get_host(process_id, ["MASTER"])
    else:
        ip = get_host(process_id, ["MASTER", "CORE"])
    context = kwargs
    dag_id = str(context['dag_run'].run_id)
    data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
    cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
    logging.info("IP---------- - " + str(ip))
    # ssh = ExecuteSSHCommand(remote_host=ip, username=CommonConstants.EMR_USER_NAME,
    # key_file=private_key_location)
    command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; python3 PublishDWHandler.py" + " " + \
              str(process_id) + " " + str(frequency) + " " + str(data_date) + " " + str(cycle_id) + " " + str(dag_id)
    cluster_id = get_cluster_id(str(process_id))
    logging.info("Command to be executed:" + command)
    output = SystemManagerUtility().execute_command(cluster_id, command)
    logging.info("Output generated by PublishDWHandler :" + str(output))


def update_cycle_status_and_cleaup(process_id, frequency, **kwargs):
    """This method updates cycle status and clean up"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
    cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
    success_status = CommonConstants.STATUS_SUCCEEDED
    copy_config_rule_files_to_s3_flag = CommonConstants.COPY_JOB_EXECUTOR_CONFIG_FILES_TO_S3_FLAG
    if copy_config_rule_files_to_s3_flag.lower() == "y":
        copy_config_rule_files_to_s3(process_id=process_id, cycle_id=cycle_id, data_date=data_date)

    CommonUtils().update_cycle_audit_entry(process_id, frequency, data_date, cycle_id,
                                           success_status)
    process_date_delete_query = "DELETE from {audit_db}.{process_date_table} where " \
                                "process_id={process_id} and" \
                                " frequency='{frequency}' ". \
        format(audit_db=audit_db, process_date_table=CommonConstants.PROCESS_DATE_TABLE,
               process_id=process_id, frequency=frequency)
    logging.debug(process_date_delete_query)
    MySQLConnectionManager().execute_query_mysql(process_date_delete_query)
    logging.info("Process date mapping entry deleted for process id:" + str(process_id) + " " +
                 "and frequency:" + str(frequency))


def copy_config_rule_files_to_s3(process_id, cycle_id, data_date, cluster_id=None):
    """This method copies job executor config files to s3"""
    try:
        status_message = "Starting to execute copy_spark_logs_to_s3 for cluster id: " + \
                         str(cluster_id)
        logging.info(status_message)

        if cluster_id is None:
            cluster_id = get_cluster_id(process_id)

        configuration = JsonConfigUtility(os.path.join(CommonConstants.AIRFLOW_CODE_PATH,
                                                       CommonConstants.ENVIRONMENT_CONFIG_FILE))

        dw_s3_rule_file_history_path = configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, CommonConstants.DW_S3_RULE_FILE_HISTORY_KEY])
        dw_s3_rule_file_latest_path = configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, CommonConstants.DW_S3_RULE_FILE_LATEST_KEY])
        # private_key_location = configuration.get_configuration(
        #     [CommonConstants.ENVIRONMENT_PARAMS_KEY, CommonConstants.PRIVATE_KEY_LOACTION])
        s3_bucket_name = configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, CommonConstants.S3_BUCKET_NAME_KEY])

        copy_config_rule_files_to_s3_flag = CommonConstants.COPY_JOB_EXECUTOR_CONFIG_FILES_TO_S3_FLAG
        if dw_s3_rule_file_history_path is None or dw_s3_rule_file_latest_path is None:
            copy_config_rule_files_to_s3_flag = False
        if dw_s3_rule_file_history_path is "" or dw_s3_rule_file_latest_path is "":
            copy_config_rule_files_to_s3_flag = False
        if dw_s3_rule_file_history_path == "" or dw_s3_rule_file_latest_path == "":
            copy_config_rule_files_to_s3_flag = False

        logging.info("copy_config_rule_files_to_s3_flag is : " + str(copy_config_rule_files_to_s3_flag))
        if copy_config_rule_files_to_s3_flag.lower() == "y":
            dw_s3_rule_file_history_path = str(dw_s3_rule_file_history_path).rstrip("/") + "/"
            dw_s3_rule_file_latest_path = str(dw_s3_rule_file_latest_path).rstrip("/") + "/"
            master_ip = CommonUtils().get_master_ip_from_cluster_id(cluster_id)

            # default_job_executor_config_files_location = CommonConstants.JOB_EXECUTOR_CONFIG_FILES_DEFAULT_PATH_EMR
            default_job_executor_config_files_location = os.path.join(CommonConstants.EMR_CODE_PATH,
                                                                      "configs/job_executor")
            job_executor_config_files_s3_path_history = str("s3://") + s3_bucket_name + '/' + str(
                dw_s3_rule_file_history_path).format(process_id=process_id, cycle_id=cycle_id)
            job_executor_config_files_s3_path_latest = str("s3://") + s3_bucket_name + '/' + str(
                dw_s3_rule_file_latest_path).format(process_id=process_id,
                                                    cycle_id=cycle_id)
            logging.info("job_executor_config_files_s3_path_history: " + job_executor_config_files_s3_path_history)
            logging.info("job_executor_config_files_s3_path_latest: " + job_executor_config_files_s3_path_latest)
            s3_resource = boto3.resource("s3")

            s3_bucket = s3_resource.Bucket(s3_bucket_name)
            prefix = \
                str(dw_s3_rule_file_history_path).format(process_id=process_id, cycle_id=cycle_id).rsplit("/",
                                                                                                          maxsplit=1)[
                    0]
            s3_bucket.objects.filter(Prefix=prefix).delete()
            if default_job_executor_config_files_location:
                command_to_execute = "aws s3 cp --recursive {default_job_executor_config_files_location} " \
                                     "{job_executor_config_files_s3_path_history} " \
                                     "; aws s3 cp --recursive {default_job_executor_config_files_location} " \
                                     "{job_executor_config_files_s3_path_latest}".format(
                                         default_job_executor_config_files_location=default_job_executor_config_files_location,
                                         job_executor_config_files_s3_path_history=job_executor_config_files_s3_path_history,
                                         job_executor_config_files_s3_path_latest=job_executor_config_files_s3_path_latest
                )

                status_message = "Command created for file transfer: " + str(command_to_execute)
                logging.info(status_message)

                # ssh = ExecuteSSHCommand(remote_host=master_ip, username=CommonConstants.EMR_USER_NAME,
                # key_file=private_key_location)
                output = SystemManagerUtility().execute_command(cluster_id, command_to_execute)
                logging.info("Output generated while copying the files from EMR master node to s3: " + str(output))
            elif default_job_executor_config_files_location == "" or default_job_executor_config_files_location is None:
                logging.info("Config rule files path is not provided. Nothing to copy")

        status_message = "Copied the config rule files to S3"
        logging.info(status_message)


    except Exception as exception:
        status_message = "Error occurred while copying the job executor config rule files from EMR master node " \
                         "to s3. ERROR: " + str(exception)
        logger.error(str(traceback.format_exc()))
        logger.error(status_message)
        raise exception


def launch_cluster_dw(process_id, frequency, **kwargs):
    """This method launches dw cluster"""
    try:
        logging.info("Cluster launch invoked for process_id:" + str(process_id) + " " +
                     "and frequency:" + str(frequency))
        launch_emr(str(process_id), **kwargs)
        cluster_id = get_cluster_id(str(process_id))
        logging.info("Cluster launch completed for process_id:" + str(process_id) + " " +
                     "and frequency:" + str(frequency))
        data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
        logging.info("Data date is " + str(data_date))
        cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
        logging.info("Cycle Id is " + str(cycle_id))
        CommonUtils().update_cycle_cluster_id_entry(process_id, frequency, data_date, cycle_id,
                                                    cluster_id)
    except Exception as exception:
        logging.error("Cluster launch failed for process_id:" + str(process_id) + " " +
                      "and frequency:" + str(frequency))
        logger.error(str(traceback.format_exc()))
        failed_status = CommonConstants.STATUS_FAILED
        CommonUtils().update_cycle_audit_entry(process_id, frequency, data_date, cycle_id,
                                               failed_status)
        raise exception


def fetch_batch_id_prelanding(cluster_id, process_id, dataset_id, workflow_id):
    """This method fetches batch id"""
    logger.info("Starting function to retrieve the Batch ID of current execution")
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
    query = "select group_concat(batch_id separator ',') as batch_id from " + audit_db + \
            "." + CommonConstants.BATCH_TABLE + " where process_id = " + str(
        process_id) + " and cluster_id = '" + str(cluster_id) + "' and dataset_id=" + str(
        dataset_id) + " and workflow_id = '" + str(workflow_id) + "'"
    logger.info("Query to retrieve the batch ID: " + query)
    query_output = MySQLConnectionManager().execute_query_mysql(query, False)
    logger.info("Query Output : " + str(query_output))
    return query_output[0]["batch_id"]


def get_dataset_lineage(file_master_id, dag_name, **kwargs):
    try:
        ti = kwargs["ti"]
        batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                               dag_id=dag_name)
        batch_ids = batches.split(",")
        process_name = CommonConstants.CC_INGESTION_LINEAGE_PROCESS
        for batch_id in batch_ids:
            lineage_obj = IngestionLineageWrapper(process_name, dataset_id=file_master_id, batch_id=batch_id)
            lineage_obj.get_lineage()
    except Exception as exc:
        logger.error(traceback.print_exc())
        raise exc


def get_latest_cycle_id_by_process(process_id):
    try:
        configuration = JsonConfigUtility(
            os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
        audit_db = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
        query = "select cycle_id from {db}.{tbl_nm} where process_id='{process_id}' and cycle_status = '{status}' order by cycle_end_time desc limit 1".format(
            tbl_nm=CommonConstants.LOG_CYCLE_DTL,
            db=audit_db,
            process_id=process_id,
            status=CommonConstants.STATUS_SUCCEEDED
        )
        logger.info("query to get_latest_cycle_id_by_process is {query}".format(query=query))
        result = MySQLConnectionManager().execute_query_mysql(query, True)
        return result["cycle_id"]
    except Exception as exc:
        logger.error(traceback.print_exc())
        raise exc


def get_dw_lineage(process_id, **kwargs):
    try:

        cycle_id = get_latest_cycle_id_by_process(process_id)
        configuration = JsonConfigUtility(
            os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
        dw_s3_rule_file_latest = configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, "dw_s3_rule_file_latest"])
        s3_bucket = configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "s3_bucket_name"])
        RULE_FILES_S3_PATH = "s3://{s3_bucket}/{dw_s3_rule_file_latest}".format(
            s3_bucket=s3_bucket,
            dw_s3_rule_file_latest=dw_s3_rule_file_latest.format(cycle_id=cycle_id, process_id=process_id)
        )
        logger.info("RULE_FILES_S3_PATH===>>>{RULE_FILES_S3_PATH}".format(
            RULE_FILES_S3_PATH=RULE_FILES_S3_PATH
        ))
        dw_lineage_wrapper_obj = DWLineageWrapper()
        dw_lineage_wrapper_obj.main(rule_files_s3_path=RULE_FILES_S3_PATH, cycle_id=cycle_id)
    except Exception as exc:
        logger.error(traceback.print_exc())
        raise exc



def dqm_dw(process_id, frequency, process_name=None, **kwargs):
    """This method performs DQM check on DW tables"""
    configuration = JsonConfigUtility(
        os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
    if CommonConstants.CLUSTER_MASTER_SUBMIT_FLAG == CommonConstants.ACTIVE_IND_VALUE:
        ip = get_host(process_id, ["MASTER"])
    else:
        ip = get_host(process_id, ["MASTER", "CORE"])
    data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
    cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
    logging.info("IP---------- - " + str(ip))
    cluster_id = get_cluster_id(process_id)
    command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; /usr/lib/spark/bin/spark-submit DQMCheckModule.py" + \
              " " + str(process_id) + " " + str(frequency) + " " + str(process_name)
    logging.info("Command to be executed:" + command)
    output = SystemManagerUtility().execute_command(cluster_id, command)
    logging.info("Output generated by DQMCheckModule :" + str(output))


def update_catalog_data(file_master_id, dag_name, process_id, **kwargs):
    try:
        ti = kwargs["ti"]
        batches = ti.xcom_pull(key='batch_ids', task_ids="pre_landing_" + str(file_master_id),
                               dag_id=dag_name)
        batch_ids = batches.split(",")
        logging.info("Batch Id fetched from pre landing is - " + str(batches))

        conf_value = get_spark_configurations(file_master_id, process_id)
        cluster_id = get_cluster_id(process_id)
        ip = get_host(process_id, ["MASTER"])
        logging.info("cluster_id---------- - " + str(cluster_id))

        context = kwargs
        dag_id = str(context['dag_run'].run_id)

        steps = ["landing", "pre_dqm", "staging"]
        if CommonConstants.PUBLISH_TYPE.lower() == 'view':
            steps.append('publish_view')
        else:
            steps.append("publish_table")

        steps = ",".join(steps)
        for batch_id in batch_ids:
            processed_flag = CommonUtils().check_if_processed_batch_entry(file_master_id, batch_id, cluster_id,
                                                                          process_id)
            if processed_flag:
                break
            # crawler_obj = LaunchDDLCreationIngestionHandler(dataset_id=file_master_id, step=step)
            # crawler_obj.get_table_details()
            # crawler_obj.call_add_partition_data(batch_id=batch_id)
            if conf_value.find('--deploy-mode cluster') != -1:
                logger.info("================Executing job in Spark cluster mode================")
                ##Adding mandatory max attempt as 1
                conf_value = conf_value + " --conf spark.yarn.maxAppAttempts=1"
                conf_value = conf_value + " --name LaunchDDLCreationIngestionHandler_" + file_master_id + "_" + batch_id
                cmd = "cd {airflow_code_path};/usr/lib/spark/bin/spark-submit {conf_value} LaunchDDLCreationIngestionHandler.py " \
                      "--dataset_id {file_master_id} --step {step} --batch_id {batch_id}".format(
                    conf_value=conf_value,
                    file_master_id=file_master_id,
                    step=steps,
                    batch_id=batch_id,
                    airflow_code_path=CommonConstants.AIRFLOW_CODE_PATH
                )
                try:
                    output = SystemManagerUtility(log_type='stderr').execute_command(cluster_id,
                                                                                     cmd)
                    logging.info("Output generated by Update catalog task - " + str(output))
                except Exception as exception:
                    status_message = "Spark Submit failed due to" + str(exception)
                    logger.error(status_message)
                finally:
                    response = CommonUtils().get_yarn_app_id_status(
                        app_name="LaunchDDLCreationIngestionHandler_" + file_master_id,
                        cluster_ip=ip)
                    yarn_app_id = response["app_id"]
                    yarn_app_state = response["state"]
                    yarn_diagnostics_info = response["diagnostic_message"]
                    logger.error("Diagnostics Info:" + yarn_diagnostics_info)
                    output_cmnd = "yarn logs -applicationId " + str(yarn_app_id)
                    logger.info("This RUN was configured in cluster mode, To get full logs run the "
                                "following command on EMR {yarn_log_command}".format(
                        yarn_log_command=output_cmnd
                    ))

                    CommonUtils().set_inprogress_records_to_failed(step_name=CommonConstants.FILE_PROCESS_NAME_DQM,
                                                                   batch_id=batch_id)
                    if yarn_app_state == 'FAILED' or yarn_app_state == 'KILLED':
                        status_message = "Spark Job Failed"
                        raise Exception(status_message)

            else:
                logger.info("================Executing job in Spark client mode================")
                cmd = "cd {airflow_code_path};/usr/lib/spark/bin/spark-submit {conf_value} LaunchDDLCreationIngestionHandler.py " \
                      "--dataset_id {file_master_id} --step {step} --batch_id {batch_id}".format(
                    conf_value=conf_value,
                    file_master_id=file_master_id,
                    step=steps,
                    batch_id=batch_id,
                    airflow_code_path=CommonConstants.AIRFLOW_CODE_PATH
                )
                logger.info(cmd)
                output = SystemManagerUtility().execute_command(cluster_id,
                                                                cmd)
                logging.info("Output generated by Update catalog task - " + str(output))
    except Exception as exc:
        logger.error(str(exc))
        raise exc


def update_catalog_data_dw(process_id, frequency, **kwargs):
    try:
        """This method launches ddl creation on glue for dw process"""
        configuration = JsonConfigUtility(
            os.path.join(CommonConstants.AIRFLOW_CODE_PATH, CommonConstants.ENVIRONMENT_CONFIG_FILE))
        cluster_id = get_cluster_id(str(process_id))
        if CommonConstants.CLUSTER_MASTER_SUBMIT_FLAG == CommonConstants.ACTIVE_IND_VALUE:
            ip = get_host(process_id, ["MASTER"])
        else:
            ip = get_host(process_id, ["MASTER", "CORE"])
        data_date = CommonUtils().fetch_data_date_process(process_id, frequency)
        cycle_id = CommonUtils().fetch_cycle_id_for_data_date(process_id, frequency, data_date)
        logging.info("IP---------- - " + str(ip))

        steps = ["staging_dw"]
        if CommonConstants.PUBLISH_TYPE.lower() == 'view':
            steps.append('publish_dw_view')
        else:
            steps.append("publish_dw_table")

        steps = ",".join(steps)
        ##Get Target Dataset ID's for process_id
        target_datasets_result = CommonUtils().get_target_datasets_by_process_id(process_id=process_id)
        logger.info("target_datasets_result:{0}".format(target_datasets_result))
        target_datasets_list = []
        for row in target_datasets_result:
            target_datasets_list.append(str(row['dataset_id']))

        target_datasets_list = ','.join(target_datasets_list)

        command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; /usr/lib/spark/bin/spark-submit LaunchDDLCreationIngestionHandler.py " + \
                  "--dataset_id {file_master_id} --step {step} --cycle_id {cycle_id} --data_dt {data_dt}".format(
                      file_master_id=target_datasets_list,
                      step=steps,
                      cycle_id=cycle_id,
                      data_dt=data_date
                  )
        logging.info("Command to be executed:" + command)
        output = SystemManagerUtility().execute_command(cluster_id, command)
        logging.info("Output generated by LaunchDDLCreationIngestionHandler :" + str(output))
    except Exception as exc:
        logger.error(str(exc))
        raise exc


def call_ftp_pre_ingestion(source_type, src_system, process_id, **kwargs):
    """
    This function calls ftp pre ingestion
    """
    try:
        ip = get_host(process_id, ["MASTER"])
        cluster_id = get_cluster_id(process_id)
        logging.info("IP---------- - " + str(ip))
        execution_command = "cd " + CommonConstants.AIRFLOW_CODE_PATH + "; python3 FTPMoveCopyWrapper.py -sp " + \
                            source_type + " -sc " + src_system
        logger.info(execution_command)
        output = SystemManagerUtility().execute_command(cluster_id, execution_command)
        logger.info("Output generated by ftp pre-ingestion task - " + str(output))

    except Exception as exc:
        logger.error(str(traceback.format_exc()))
        logger.error("Error: " + str(exc))
        logger.info("Some metadata information for DW email is not configured")

def call_rdbms_pre_ingestion(source_type, process_id, src_system=None, database=None, table=None, threads=None,
                          **kwargs):
    """
    This function calls rdbms pre ingestion
    """
    try:
        if source_type.lower() == CommonConstants.RDBMS_RDS_TYPE.lower():
            drivers = CommonConstants.RDBMS_MYSQL_JDBC_JAR_FILE_PATH.replace(" ", "")
        elif source_type.lower() == CommonConstants.RDBMS_TERADATA_TYPE.lower():
            drivers = CommonConstants.RDBMS_TERADATA_JDBC_JAR_FILE_PATH.replace(" ", "")
        elif source_type.lower() == CommonConstants.RDBMS_ORACLE_TYPE.lower():
            drivers = CommonConstants.RDBMS_ORACLE_JDBC_JAR_FILE_PATH.replace(" ", "")
        else:
            raise Exception("Please provide appropriate source_tyep (rds/oracle/teradata)")
        parameters = ""
        if src_system is not None:
            parameters = parameters + " -sc '{src_system}' ".format(src_system=src_system)
        if database is not None:
            parameters = parameters + " -db '{database}' ".format(database=database)
        if table is not None:
            parameters = parameters + " -tbl '{table}' ".format(table=table)
        if threads is not None:
            parameters = parameters + " -th '{threads}' ".format(threads=threads)
        ip = get_host(process_id, ["MASTER"])
        cluster_id = get_cluster_id(process_id)
        logging.info("IP---------- - " + str(ip))
        logger.info("================Executing job in Spark client mode================")
        execution_command = "cd {code_path}; /usr/lib/spark/bin/spark-submit --jars {drivers} RDBMSCopyWrapper.py " \
                            " {parameters}".format(
            code_path=CommonConstants.AIRFLOW_CODE_PATH,
            drivers=drivers, parameters=parameters)
        logger.info(execution_command)
        output = SystemManagerUtility().execute_command(cluster_id, execution_command)
        logger.info("Output generated by rdbms pre-ingestion task - " + str(output))

    except Exception as exc:
        logger.error(str(traceback.format_exc()))
        logger.error("Error: " + str(exc))
        logger.info("Some metadata information for DW email is not configured")


