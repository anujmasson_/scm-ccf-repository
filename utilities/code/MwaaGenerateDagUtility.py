#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ZS Associates'

# This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.

######################################################Module Information################################################
#   Module Name         :   MwaaGenerateDagUtility
#   Purpose             :   Generate and copy the dag in s3
#   Input Parameters    :   process_id, account_id, airflow_env_name, assumed_role_arn
#   Output              :   MWAA Webserver Link
#   Execution Steps     :   Instantiate and object of this class and call the class methods
#   Predecessor module  :   This module is specific to MWAA
#   Successor module    :   NA
#   Developed on        :   11 Nov 2021
#   Developed by        :   Sukhwinder Singh
#   Last changed on     :   NA
#   Last changed by     :   NA
#   Reason for change   :   NA
########################################################################################################################

import os
import json
import base64
import time
import requests
import argparse
import traceback
from ConfigUtility import JsonConfigUtility
import CommonConstants as CommonConstants
from MwaaCommonUtilities import MwaaCommonUtilities
from ccf_logging import logger

MODULE_NAME = "MwaaGenerateDagUtility"

class GenerateDag(object):
    def __init__(self, process_id, airflow_env_name, assumed_role_arn, process_type, dag_concurrency=None):
        """
        :param process_id: Process id for which dag need to be run
        :param airflow_env_name: MWAA Environment Name
        :param assumed_role_arn: ARN of assumed role
        :param process_type: Type of process which will be executed using dag (Ingestion/DW)
        :return: Webserver link
        """
        self.process_id = process_id
        self.airflow_env_name = airflow_env_name
        self.assumed_role_arn = assumed_role_arn
        self.process_type = process_type
        self.dag_concurrency = dag_concurrency
        logger.info("The provided process_id is: {0}".format(process_id))
        logger.info("The provided airflow_env_name is: {0}".format(airflow_env_name))
        logger.info("The provided assumed_role_arn is: {0}".format(assumed_role_arn))
        logger.info("The provided process_type is: {0}".format(process_type))
        logger.info("The provided dag_concurrency is: {0}".format(dag_concurrency))
        self.configuration = JsonConfigUtility(
            CommonConstants.AIRFLOW_CODE_PATH + '/' + CommonConstants.ENVIRONMENT_CONFIG_FILE)
        self.mysql_database = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
        self.aws_region = self.configuration.get_configuration(
            [CommonConstants.ENVIRONMENT_PARAMS_KEY, "aws_region"])
        self.utility_obj = MwaaCommonUtilities(role_arn=self.assumed_role_arn)
        # self.logger = self.utility_obj.get_logger()

    def get_mwaa_dag_folder_path(self):
        """
        Get the dag folder path of MWAA
        """
        response = dict()
        try:
            logger.info("Getting the DAG folder path of MWAA")
            mwaa_client_response = self.utility_obj.create_mwaa_client()
            if mwaa_client_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(mwaa_client_response)
            mwaa_client = mwaa_client_response[CommonConstants.RESULT_KEY]
            response = mwaa_client.get_environment(Name=self.airflow_env_name)
            s3_bucket_arn = response['Environment']['SourceBucketArn'].rsplit(":")[-1]
            folder_path = response['Environment']['DagS3Path']
            s3_folder_path = "s3://" + s3_bucket_arn + "/" + folder_path.strip("/") + "/"
            logger.info("The DAG folder path of MWAA is: {0}".format(s3_folder_path))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = s3_folder_path
            return response
        except Exception as exp:
            error = "Failed while getting the MWAA dag folder path. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def copy_dag_file_to_mwaa(self, source_path, target_path):
        """
        Copy the dag file from source path to target path
        :param source_path: Source path of dag
        :param target_path: Target path where dag need to be copied
        :return:
        """
        response = dict()
        try:
            logger.info("Copying the dag file")
            copy_command = "aws s3 cp {source_path} {target_path} --acl " \
                           " bucket-owner-full-control".format(source_path=source_path,
                                                               target_path=target_path)
            logger.info(copy_command)
            copy_command_res = self.utility_obj.execute_shell_command(command=copy_command)
            if copy_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                copy_command_error = copy_command_res[CommonConstants.ERROR_KEY]
                status_message = "Exception in copying the dag file {0}".format(str(copy_command_error))
                raise Exception(status_message)
            else:
                copy_command_result = copy_command_res[CommonConstants.RESULT_KEY]
                logger.info(copy_command_result)
            logger.info("Copied the dag file '{0}' to '{1}'".format(source_path, target_path))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            return response
        except Exception as exp:
            error = "Failed while copying the dag in s3. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def get_upstream_dependencies(self, rule_configs):
        """
        Prepare the dag execution of rules stages based on the upstream dependencies
        :param rule_configs: Rule configs present in rule config table
        :return: The string having rule stages
        """
        response = dict()
        try:
            rule_stage = ""
            for rule_config in rule_configs:
                if "upstream_dependency" not in rule_config.keys() or rule_config["upstream_dependency"] == "" or \
                        rule_config[
                            "upstream_dependency"] is None:
                    rule_stage = rule_stage + "\\n" + "{0}.set_upstream(launch_ddl_creation)".format(
                        rule_config["step_name"])
                if "upstream_dependency" in rule_config.keys() and (
                        rule_config["upstream_dependency"] != "" and rule_config["upstream_dependency"] is not None):
                    dependencies = rule_config["upstream_dependency"].split(",")
                    if len(dependencies) == 1:
                        rule_stage = rule_stage + "\\n" + "{0}.set_upstream({1})".format(rule_config["step_name"],
                                                                                        rule_config[
                                                                                            "upstream_dependency"])
                    else:
                        for dependency in dependencies:
                            rule_stage = rule_stage + "\\n" + "{0}.set_upstream({1})".format(rule_config["step_name"],
                                                                                            dependency.strip())
            logger.info("The rule execution steps according to upstream dependency are: \n {0}".format(rule_stage))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = rule_stage
            return response

        except Exception as exp:
            error = "Failed while preparing the upstream dependencies for rules. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def get_publish_dependencies(self, rule_configs):
        """
        Prepare the dag execution of publish step based on the upstream dependencies of rules
        :param rule_configs: Rule configs present in rule config table
        :return: The string having publish stages
        """
        response = dict()
        try:
            publish_dependency = ""
            steps = set()
            update_dependencies = set()
            for rule_config in rule_configs:
                steps.add(rule_config["step_name"])
                if "upstream_dependency" in rule_config.keys():
                    for m in rule_config["upstream_dependency"].split(","):
                        update_dependencies.add(m.strip())
            publish_dependency_list = set(steps) - set(update_dependencies)
            for dependency in publish_dependency_list:
                publish_dependency = publish_dependency + "\\n" + "publish.set_upstream({0})".format(dependency)
            logger.info("The publish execution step according to upstream dependency of rules are: \n {0}".format(publish_dependency))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = publish_dependency
            return response

        except Exception as exp:
            error = "Failed while preparing the publish dependencies. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def get_rule_steps(self, rule_configs):
        """
        Prepare the dag stages for rules based on the upstream dependencies present in rule config table
        :param rule_configs: Rule configs present in rule config table
        :return: The string having rule stages
        """
        response = dict()
        try:
            rule_string = """$$step = PythonOperator(task_id="$$step", \\n    python_callable=dagutils.call_job_executor, \\n    op_kwargs={"process_id": PROCESS_ID, "frequency": "$$freq", "step_name": "$$step"}, \\n    dag=dag)"""
            rule_set = ""
            for rule in rule_configs:
                step_name = rule["step_name"]
                freq = rule["frequency"]
                rule_set = rule_set + "\\n\\n" + rule_string.replace("$$step", step_name).replace("$$freq", freq)
            logger.debug("The rule steps are: \n {0}".format(rule_set))
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            response[CommonConstants.RESULT_KEY] = rule_set
            return response

        except Exception as exp:
            error = "Failed while preparing the rule steps. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def create_ingestion_dag(self, python_file, dataset_id, process_name):
        """
        Ingestion dag file will be created
        :param python_file: file name of ingestion dag
        :param dataset_id: dataset ids present in workflow master table for the ingestion process
        :param process_name: process name of ingestion dag
        :return:
        """
        response = dict()
        try:
            logger.info("Preparing the Ingestion dag")
            dag_template_name = CommonConstants.INGESTION_DAG_TEMPLATE
            data_set = "sed -i 's|$$Dataset_Id_List|" + json.dumps(dataset_id) + "|g' " + python_file
            python_scripts_path = "sed -i 's|$$python_scripts_path|" + \
                                  CommonConstants.AIRFLOW_CODE_PATH + "|g' " + python_file
            dag_name = "sed -i 's|$$dagname|" + process_name + "|g' " + python_file
            p_id = "sed -i 's|$$p_id|" + self.process_id + "|g' " + python_file
            if self.dag_concurrency is None:
                dag_concurrency = "sed -i 's|dag_concurrency|" + "concurrency=" + \
                                  CommonConstants.DAG_CONCURRENCY_DEFAULT + "|g' " + python_file
            elif self.dag_concurrency == 0:
                dag_concurrency = "sed -i 's|dag_concurrency|" + "" + "|g' " + python_file
            else:
                dag_concurrency = "sed -i 's|dag_concurrency|" + "concurrency=" + self.dag_concurrency + "|g' " + python_file

            if os.path.exists(CommonConstants.AIRFLOW_CODE_PATH) is True:
                code_path_command = "cd" + " " + CommonConstants.AIRFLOW_CODE_PATH
                code_path_command_res = self.utility_obj.execute_shell_command(command=code_path_command)
                if code_path_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    code_path_command_error = code_path_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(code_path_command_error))
                    raise Exception(status_message)
                else:
                    code_path_command_result = code_path_command_res[CommonConstants.RESULT_KEY]
                    logger.info(code_path_command_result)

                copy_dag_command = "cp" + " " + str(dag_template_name) + " " + python_file
                copy_dag_command_res = self.utility_obj.execute_shell_command(command=copy_dag_command)
                if copy_dag_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    copy_dag_command_error = copy_dag_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(copy_dag_command_error))
                    raise Exception(status_message)
                else:
                    copy_dag_command_result = copy_dag_command_res[CommonConstants.RESULT_KEY]
                    logger.info(copy_dag_command_result)

                p_id_command_res = self.utility_obj.execute_shell_command(command=p_id)
                if p_id_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    p_id_command_error = p_id_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(p_id_command_error))
                    raise Exception(status_message)
                else:
                    p_id_command_result = p_id_command_res[CommonConstants.RESULT_KEY]
                    logger.info(p_id_command_result)

                data_set_command_res = self.utility_obj.execute_shell_command(command=data_set)
                if data_set_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    data_set_command_error = data_set_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(data_set_command_error))
                    raise Exception(status_message)
                else:
                    data_set_command_result = data_set_command_res[CommonConstants.RESULT_KEY]
                    logger.info(data_set_command_result)

                dag_concurrency_command_res = self.utility_obj.execute_shell_command(command=dag_concurrency)
                if dag_concurrency_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    dag_concurrency_command_error = dag_concurrency_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(dag_concurrency_command_error))
                    raise Exception(status_message)
                else:
                    dag_concurrency_command_result = dag_concurrency_command_res[CommonConstants.RESULT_KEY]
                    logger.info(dag_concurrency_command_result)

                python_scripts_path_command_res = self.utility_obj.execute_shell_command(command=python_scripts_path)
                if python_scripts_path_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    python_scripts_path_command_error = code_path_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(python_scripts_path_command_error))
                    raise Exception(status_message)
                else:
                    python_scripts_path_command_result = code_path_command_res[CommonConstants.RESULT_KEY]
                    logger.info(python_scripts_path_command_result)

                dag_name_command_res = self.utility_obj.execute_shell_command(command=dag_name)
                if dag_name_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    dag_name_command_error = dag_name_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(dag_name_command_error))
                    raise Exception(status_message)
                else:
                    dag_name_command_result = code_path_command_res[CommonConstants.RESULT_KEY]
                    logger.info(dag_name_command_result)

            logger.info("Created the Ingestion dag file successfully")
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            return response

        except Exception as exp:
            error = "Failed while generating the Ingestion dag file. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def create_dw_dag(self, python_file, process_name, frequency):
        """
        DW dag file will be created
        :param python_file: file name of DW dag
        :param frequency: frequency for DW process
        :param process_name: process name of DW dag
        :return:
        """
        response = dict()
        try:
            logger.info("Preparing the DW dag")
            dag_template_name = CommonConstants.DW_DAG_TEMPLATE
            get_rule_configs_response = self.utility_obj.get_rule_configs(audit_db=self.mysql_database,
                                                                          process_id=self.process_id)
            if get_rule_configs_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(get_rule_configs_response)
            rule_configs = get_rule_configs_response[CommonConstants.RESULT_KEY]
            print(rule_configs)
            get_rule_steps_response = self.get_rule_steps(rule_configs=rule_configs)
            if get_rule_steps_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(get_rule_steps_response)
            rule_steps = get_rule_steps_response[CommonConstants.RESULT_KEY]
            print(rule_steps)

            get_upstream_dependencies_response = self.get_upstream_dependencies(rule_configs=rule_configs)
            if get_upstream_dependencies_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(get_upstream_dependencies_response)
            upstream_dependencies = get_upstream_dependencies_response[CommonConstants.RESULT_KEY]
            print(upstream_dependencies)
            get_publish_dependencies_response = self.get_publish_dependencies(rule_configs=rule_configs)
            if get_publish_dependencies_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(get_publish_dependencies_response)
            publish_dependencies = get_publish_dependencies_response[CommonConstants.RESULT_KEY]
            python_scripts_path = "sed -i 's|$$python_scripts_path|" + \
                                  CommonConstants.AIRFLOW_CODE_PATH + "|g' " + python_file
            dag_name = "sed -i 's|$$dagname|" + process_name + "|g' " + python_file
            p_id = "sed -i 's|$$p_id|" + self.process_id + "|g' " + python_file
            frequency = "sed -i 's|$$frequency|" + frequency + "|g' " + python_file
            rules_set = "sed -i 's|$$rules_set|" + rule_steps + "|g' " + python_file
            upstream_dependencies_set = "sed -i 's|$$rules_upstream_dependencies|" + upstream_dependencies + "|g' " + python_file
            publish_dependencies_set = "sed -i 's|$$rules_publish_dependencies|" + publish_dependencies + "|g' " + python_file
            if self.dag_concurrency is None:
                dag_concurrency = "sed -i 's|dag_concurrency|" + "concurrency=" + \
                                  CommonConstants.DAG_CONCURRENCY_DEFAULT + "|g' " + python_file
            elif self.dag_concurrency == 0:
                dag_concurrency = "sed -i 's|dag_concurrency|" + "" + "|g' " + python_file
            else:
                dag_concurrency = "sed -i 's|dag_concurrency|" + "concurrency=" + self.dag_concurrency + "|g' " + python_file

            if os.path.exists(CommonConstants.AIRFLOW_CODE_PATH) is True:
                code_path_command = "cd" + " " + CommonConstants.AIRFLOW_CODE_PATH
                code_path_command_res = self.utility_obj.execute_shell_command(command=code_path_command)
                if code_path_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    code_path_command_error = code_path_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(code_path_command_error))
                    raise Exception(status_message)
                else:
                    code_path_command_result = code_path_command_res[CommonConstants.RESULT_KEY]
                    logger.info(code_path_command_result)

                copy_dag_command = "cp" + " " + str(dag_template_name) + " " + python_file
                copy_dag_command_res = self.utility_obj.execute_shell_command(command=copy_dag_command)
                if copy_dag_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    copy_dag_command_error = copy_dag_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(copy_dag_command_error))
                    raise Exception(status_message)
                else:
                    copy_dag_command_result = copy_dag_command_res[CommonConstants.RESULT_KEY]
                    logger.info(copy_dag_command_result)

                p_id_command_res = self.utility_obj.execute_shell_command(command=p_id)
                if p_id_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    p_id_command_error = p_id_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(p_id_command_error))
                    raise Exception(status_message)
                else:
                    p_id_command_result = p_id_command_res[CommonConstants.RESULT_KEY]
                    logger.info(p_id_command_result)

                frequency_command_res = self.utility_obj.execute_shell_command(command=frequency)
                if frequency_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    frequency_command_error = frequency_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(frequency_command_error))
                    raise Exception(status_message)
                else:
                    frequency_command_result = frequency_command_res[CommonConstants.RESULT_KEY]
                    logger.info(frequency_command_result)

                dag_concurrency_command_res = self.utility_obj.execute_shell_command(command=dag_concurrency)
                if dag_concurrency_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    dag_concurrency_command_error = dag_concurrency_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(dag_concurrency_command_error))
                    raise Exception(status_message)
                else:
                    dag_concurrency_command_result = dag_concurrency_command_res[CommonConstants.RESULT_KEY]
                    logger.info(dag_concurrency_command_result)

                python_scripts_path_command_res = self.utility_obj.execute_shell_command(command=python_scripts_path)
                if python_scripts_path_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    python_scripts_path_command_error = code_path_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(python_scripts_path_command_error))
                    raise Exception(status_message)
                else:
                    python_scripts_path_command_result = code_path_command_res[CommonConstants.RESULT_KEY]
                    logger.info(python_scripts_path_command_result)

                dag_name_command_res = self.utility_obj.execute_shell_command(command=dag_name)
                if dag_name_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    dag_name_command_error = dag_name_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(dag_name_command_error))
                    raise Exception(status_message)
                else:
                    dag_name_command_result = code_path_command_res[CommonConstants.RESULT_KEY]
                    logger.info(dag_name_command_result)

                rules_set_command_res = self.utility_obj.execute_shell_command(command=rules_set)
                if rules_set_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    rules_set_command_error = rules_set_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(rules_set_command_error))
                    raise Exception(status_message)
                else:
                    rules_set_command_result = rules_set_command_res[CommonConstants.RESULT_KEY]
                    logger.info(rules_set_command_result)

                upstream_dependencies_set_command_res = self.utility_obj.execute_shell_command(command=upstream_dependencies_set)
                if upstream_dependencies_set_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    upstream_dependencies_set_command_error = upstream_dependencies_set_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(upstream_dependencies_set_command_error))
                    raise Exception(status_message)
                else:
                    upstream_dependencies_set_command_result = upstream_dependencies_set_command_res[CommonConstants.RESULT_KEY]
                    logger.info(upstream_dependencies_set_command_result)

                publish_dependencies_set_command_res = self.utility_obj.execute_shell_command(command=publish_dependencies_set)
                if publish_dependencies_set_command_res[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    publish_dependencies_set_command_error = publish_dependencies_set_command_res[CommonConstants.ERROR_KEY]
                    status_message = "Exception in copying the dag file {0}".format(str(publish_dependencies_set_command_error))
                    raise Exception(status_message)
                else:
                    publish_dependencies_set_command_result = publish_dependencies_set_command_res[CommonConstants.RESULT_KEY]
                    logger.info(publish_dependencies_set_command_result)

            logger.info("Created the DW dag file successfully")
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            return response
        except Exception as exp:
            error = "Failed while generating the DW dag file. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def validate_dag(self, process_name):
        """
        Validate the dag file in MWAA Environment
        :param process_name: process name of ingestion dag
        :return:
        """
        response = dict()
        try:
            logger.info("Validating the dag")
            is_retry = True
            retry_count = 0
            mwaa_response = None
            while is_retry and retry_count < CommonConstants.MWAA_VALIDATION_TRIES:
                try:
                    time.sleep(5)
                    mwaa_client_response = self.utility_obj.create_mwaa_client()
                    if mwaa_client_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                        raise Exception(mwaa_client_response)
                    mwaa_client = mwaa_client_response[CommonConstants.RESULT_KEY]
                    token_response = mwaa_client.create_cli_token(Name=self.airflow_env_name)
                    unpause_cmd = "dags unpause {0}".format(process_name)
                    endpoint = token_response["WebServerHostname"]
                    jwt_token = token_response["CliToken"]
                    mwaa_cli_endpoint = 'https://{0}/aws_mwaa/cli'.format(endpoint)
                    mwaa_response = requests.post(mwaa_cli_endpoint,
                                                  headers={'Authorization': 'Bearer ' + jwt_token,
                                                           'Content-Type': 'text/plain'},
                                                  data=unpause_cmd)
                    logger.info(base64.b64decode(mwaa_response.json()['stderr']).decode('utf8'))
                    logger.info(base64.b64decode(mwaa_response.json()['stdout']).decode('utf8'))
                    is_retry = False
                except Exception as ex:
                    retry_count += 1
                    logger.error("Unable to validate the DAG. Retrying - " + str(retry_count) + ". Error output - " +
                                 str(base64.b64decode(mwaa_response.json()['stdout']).decode('utf8')) + " " + str(
                                     base64.b64decode(mwaa_response.json()['stderr']).decode('utf8')))
                    if retry_count == CommonConstants.RETRY_COUNT:
                        raise ex
            logger.info("Validated the dag by activating the dag")
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_SUCCESS
            return response
        except Exception as exp:
            error = "Failed while validating the dag in mwaa. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            response[CommonConstants.STATUS_KEY] = CommonConstants.STATUS_FAILED
            response[CommonConstants.RESULT_KEY] = "Error"
            response[CommonConstants.ERROR_KEY] = error
            return response

    def main(self):
        """
        Main function to create the dag
        """
        try:
            # Getting the process name and frequency from cluster config table
            process_name_response = self.utility_obj.get_process_name(audit_db=self.mysql_database, process_id=self.process_id)
            if process_name_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(process_name_response)
            process_name = process_name_response[CommonConstants.RESULT_KEY]["process_name"]
            frequency = process_name_response[CommonConstants.RESULT_KEY]["frequency"]

            # Getting the dag file name
            python_file = os.path.join(CommonConstants.AIRFLOW_CODE_PATH, process_name) + '.py'
            if self.process_type.lower() == "ingestion":

                # Getting the list of dataset ids corresponding to the process id
                dataset_id_response = self.utility_obj.get_dataset_ids(audit_db=self.mysql_database, process_id=self.process_id)
                if dataset_id_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    raise Exception(dataset_id_response)
                dataset_id = dataset_id_response[CommonConstants.RESULT_KEY]

                # Creating the ingestion dag
                ingestion_dag_response = self.create_ingestion_dag(python_file, dataset_id, process_name)
                if ingestion_dag_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    raise Exception(ingestion_dag_response)
            elif self.process_type.lower() == "dw":

                # Creating the DW dag
                dw_dag_response = self.create_dw_dag(python_file, process_name, frequency)
                if dw_dag_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                    raise Exception(dw_dag_response)
            else:
                raise Exception("Please provide the appropriate process name (Ingestion/DW)")

            # Getting the S3 Dag folder path of MWAA
            dag_folder_path_response = self.get_mwaa_dag_folder_path()
            if dag_folder_path_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(dag_folder_path_response)
            dag_folder_path = dag_folder_path_response[CommonConstants.RESULT_KEY]

            # Copying the Dag file from local path to S3 Dag folder path of MWAA
            copy_dag_response = self.copy_dag_file_to_mwaa(source_path=python_file, target_path=dag_folder_path)
            if copy_dag_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(copy_dag_response)

            # Validating the scheduling of dag file in MWAA Environment
            validate_dag_response = self.validate_dag(process_name=process_name)
            if validate_dag_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(validate_dag_response)

            # Providing the webserver link of MWAA UI
            webserver_link_response = self.utility_obj.provide_webserver_link(self.airflow_env_name)
            if webserver_link_response[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(webserver_link_response)
            webserver_link = webserver_link_response[CommonConstants.RESULT_KEY]
            logger.info("The MWAA UI Webserver link is '{0}'".format(webserver_link))
            return webserver_link
        except Exception as exp:
            error = "Failed while validating the dag in mwaa. " \
                    "ERROR is: {error}".format(error=str(exp) + '\n' + str(traceback.format_exc()))
            logger.error(error)
            raise exp

if __name__ == '__main__':
    try:
        PARSER = argparse.ArgumentParser(description="MWAA Generate Dag")
        PARSER.add_argument("-p", "--process_id",
                            help="Process id for which dag need to be run",
                            required=True)
        PARSER.add_argument("-ae", "--airflow_environment", help="MWAA Environment Name", required=True)
        PARSER.add_argument("-ra", "--role_arn", help="ARN of assumed role", required=True)
        PARSER.add_argument("-pt", "--process_type", help="Type of the process Ingestion/DW", required=True)
        PARSER.add_argument("-dc", "--dag_concurrency", help="Dag Concurrency", required=False)
        ARGS = vars(PARSER.parse_args())
        PROCESS_ID = ARGS['process_id']
        AIRFLOW_ENVIRONMENT = ARGS['airflow_environment']
        ROLE_ARN = ARGS['role_arn']
        PROCESS_TYPE = ARGS['process_type']
        DAG_CONCURRENCY = ARGS['dag_concurrency']
        obj = GenerateDag(process_id=PROCESS_ID,
                     airflow_env_name=AIRFLOW_ENVIRONMENT, assumed_role_arn=ROLE_ARN, process_type=PROCESS_TYPE,
                          dag_concurrency=DAG_CONCURRENCY)
        obj.main()
    except Exception:
        raise Exception

