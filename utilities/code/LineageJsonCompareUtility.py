import logging
import os
import traceback
from urllib.parse import urlparse
import argparse

import CommonConstants
from ConfigUtility import JsonConfigUtility
from MySQLConnectionManager import MySQLConnectionManager
from CustomS3Utility import CustomS3Utility

logger = logging.getLogger()


class LineageJsonCompareUtility:
    def __init__(self):
        if (os.path.exists(CommonConstants.AIRFLOW_CODE_PATH)):
            self.configuration = JsonConfigUtility(CommonConstants.AIRFLOW_CODE_PATH + '/' +
                                                   CommonConstants.ENVIRONMENT_CONFIG_FILE)
            #self.configuration = JsonConfigUtility(CommonConstants.ENVIRONMENT_CONFIG_FILE)
        else:
            self.configuration = JsonConfigUtility(CommonConstants.ENVIRONMENT_CONFIG_FILE)

    def compare_lineage_json(self, entity_name):
        try:

            db_name = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])
            s3_bucket_name = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "s3_bucket_name"])
            latest_run_id = self.get_run_id(db_name, entity_name)
            if not latest_run_id:
                raise Exception(f"no latest run found for entity_name {entity_name}")

            previous_loaded_run_id = self.get_run_id(db_name,entity_name,graph_load_status='Y')
            if not previous_loaded_run_id:
                return True

            new_json_path = self.get_lineage_s3_path(db=db_name, run_id=latest_run_id,entity_name=entity_name)
            #new_json_bucket = urlparse(new_json_path, allow_fragments=False).netloc
            #new_json_key = urlparse(new_json_path, allow_fragments=False).path.lstrip('/')
            #logger.info(new_json_bucket,new_json_key)

            old_json_path = self.get_lineage_s3_path(db=db_name, run_id=previous_loaded_run_id,entity_name=entity_name)
            #old_json_bucket = urlparse(old_json_path, allow_fragments=False).netloc
            #old_json_key = urlparse(old_json_path, allow_fragments=False).path.lstrip('/')
            #logger.info(old_json_bucket, old_json_key)

            new_json = CustomS3Utility()._read_json_in_s3(bucket_name=s3_bucket_name, key=new_json_path)
            if new_json[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(new_json[CommonConstants.ERROR_KEY])
            new_json = new_json[CommonConstants.RESULT_KEY]
            logger.info('New Json: {0}'.format(new_json))

            old_json = CustomS3Utility()._read_json_in_s3(bucket_name = s3_bucket_name, key = old_json_path)
            if old_json[CommonConstants.STATUS_KEY] == CommonConstants.STATUS_FAILED:
                raise Exception(old_json[CommonConstants.ERROR_KEY])
            old_json = old_json[CommonConstants.RESULT_KEY]
            logger.info('Old Json: {0}'.format(old_json))

            if new_json == old_json:
                #Setting graph load status to Y incase no graph load is necessary
                self.set_graph_load_status(db_name,latest_run_id,entity_name,'Y')
                logger.info('True')
                return False

            logger.info('False')
            return True

        except Exception as exc:
            logger.error(exc)
            raise exc

    def get_run_id(self, db, entity_name,graph_load_status=None):

        try:
            sub_query=""
            if graph_load_status:
                sub_query="and graph_load_complete = '{graph_load_status}'".format(graph_load_status=graph_load_status)
            query = "Select run_id from {db}.{lineage_dtl_table} where entity_name = '{entity_name}' and status = '{success_status}' {sub_query} order by run_id desc limit 1".format(
                db=db,
                lineage_dtl_table=CommonConstants.LINEAGE_TBL_NM,
                entity_name=entity_name,
                success_status=CommonConstants.STATUS_SUCCEEDED,
                sub_query=sub_query
            )
            result = MySQLConnectionManager().execute_query_mysql(query,True)
            if result:
                run_id=result['run_id']
                logger.info(f'Run ID -->> {run_id}')
                return run_id
            else:
                return result

        except Exception as e:
            logger.error(traceback.print_exc())
            raise e

    def get_lineage_s3_path(self, db, run_id,entity_name):
        try:
            s3_path_query = "Select s3_location from {db}.{lineage_dtl_table} where run_id = '{run_id}' and entity_name = '{entity_name}' and status = '{success_status}'".format(
                db=db,
                lineage_dtl_table=CommonConstants.LINEAGE_TBL_NM,
                run_id=run_id,
                entity_name=entity_name,
                success_status=CommonConstants.STATUS_SUCCEEDED
            )
            s3_path_result = MySQLConnectionManager().execute_query_mysql(s3_path_query, True)
            logger.info(s3_path_result)
            lineage_json_s3_path = s3_path_result["s3_location"]
            logger.info('lineage_json_s3_path for run_id {1} : {0}'.format(lineage_json_s3_path,run_id))
            return lineage_json_s3_path
        except Exception as exc:
            logger.error(exc)
            raise exc

    # def get_graph_load_status(self,db,run_id,entity_name):
    #     try:
    #         graph_load_status_query = "Select graph_load_complete from {db}.{lineage_dtl_table} where run_id = '{run_id}' and entity_name = '{entity_name}'".format(
    #             db=db,
    #             lineage_dtl_table=CommonConstants.LINEAGE_TBL_NM,
    #             run_id=run_id,
    #             entity_name=entity_name,
    #         )
    #         graph_load_status_result = MySQLConnectionManager().execute_query_mysql(graph_load_status_query, True)
    #         logger.info(graph_load_status_result)
    #         graph_load_status = graph_load_status_result["graph_load_complete"]
    #         return graph_load_status
    #     except Exception as exc:
    #         logger.error(exc)
    #         raise exc

    def set_graph_load_status(self,db,run_id,entity_name,graph_load_status):
        try:
            graph_load_status_query = "Update {db}.{lineage_dtl_table} set graph_load_complete = '{graph_load_status}' where run_id <= '{run_id}' and entity_name = '{entity_name}' and status='{success_status}'".format(
                db=db,
                lineage_dtl_table=CommonConstants.LINEAGE_TBL_NM,
                run_id=run_id,
                success_status=CommonConstants.STATUS_SUCCEEDED,
                entity_name=entity_name,
                graph_load_status=graph_load_status
            )
            graph_load_status_result = MySQLConnectionManager().execute_query_mysql(graph_load_status_query, True)
            #logger.info(graph_load_status_result)
            #graph_load_status = graph_load_status_result["graph_load_complete"]
            #return graph_load_status
            return
        except Exception as exc:
            logger.error(exc)
            raise exc



if __name__ == '__main__':
    try:
        my_parser = argparse.ArgumentParser()
        my_parser.add_argument('--entity_name', type=str, required=True)

        args = my_parser.parse_args()

        entity_name = args.entity_name

        lineageJsonCompare = LineageJsonCompareUtility()
        result = lineageJsonCompare.compare_lineage_json(entity_name=entity_name)

    except Exception as exc:
        logger.error(exc)
        raise exc