#!/usr/bin/python
# -*- coding: utf-8 -*-
# This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
import ast
# Library and external modules declaration
import os
import sys

import traceback
from LogSetup import logger
from ExecutionContext import ExecutionContext
import CommonConstants as CommonConstants
from CommonUtils import CommonUtils
from ConfigUtility import JsonConfigUtility
from PySparkUtility import PySparkUtility

sys.path.insert(0, os.getcwd())

__author__ = 'ZS Associates'

# ############################################## Module Information ####################################################
#   Module Name         : UpdateRecordCountUtility
#   Purpose             : This class is used for updating record count in log_file_dtl table
#   Input Parameters    : log info details in dictionary format
#   Output Value        :
#  Dependencies         :
#  Predecessor Module   :
#  Successor Module     :
#  Pre-requisites       : All the dependent libraries should be present in same environment from where the module would
#                         execute.
#   Last changed on     :
#   Last changed by     :
#   Reason for change   :
# ######################################################################################################################

MODULE_NAME = "UpdateRecordCountUtility"


class UpdateRecordCountUtility():
    # Initialization instance of class
    def __init__(self, parent_execution_context=None):
        # Parent execution context can be passed to the utility, in case its not; then instantiate the execution context
        if parent_execution_context is None:
            self.execution_context = ExecutionContext()
        else:
            self.execution_context = parent_execution_context
        self.execution_context.set_context({"module_name": MODULE_NAME})
        self.configuration = JsonConfigUtility(CommonConstants.ENVIRONMENT_CONFIG_FILE)

    def update_log_file_dtl(self, db_name=None, log_info=None, file_url=None, file_type=None, header=None):
        try:
            status_message = "Starting to execute record update process in log file dtl table"
            logger.debug(status_message, extra=self.execution_context.get_context())
            record_count = 0
            if file_type == "parquet":
                record_count = PySparkUtility().get_parquet_record_count(file_url)
            else:
                record_count = PySparkUtility().get_file_record_count(file_url, header)

            log_info['record_count'] = str(record_count)
            logger.debug("Record Count of file is " + str(record_count), extra=self.execution_context.get_context())
            CommonUtils().update_process_status(db_name, log_info)
            logger.debug("Updated record count in log_file_dtl table", extra=self.execution_context.get_context())
        except Exception as exc:
            error = "ERROR in " + self.execution_context.get_context_param("module_name") + " ERROR MESSAGE: " + \
                    str(traceback.format_exc())
            self.execution_context.set_context({"traceback": error})
            logger.error(status_message, extra=self.execution_context.get_context())
            raise exc


if __name__ == '__main__':
    DB_NAME = str(sys.argv[1])
    LOG_INFO = str(sys.argv[2])
    FILE_URL = str(sys.argv[3])
    FILE_TYPE = str(sys.argv[4])
    HEADER = None
    if FILE_TYPE != 'parquet':
        HEADER = str(sys.argv[5])
    LOG_INFO = ast.literal_eval(LOG_INFO)
    UpdateRecordCountUtility().update_log_file_dtl(DB_NAME, LOG_INFO, FILE_URL, FILE_TYPE, HEADER)