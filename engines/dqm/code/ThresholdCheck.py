#!/usr/bin/python
# -*- coding: utf-8 -*-
# This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
__AUTHOR__ = 'ZS Associates'

# sparksession variable is available as "spark"
import CommonConstants
from pyspark.sql.types import StructType, StructField, StringType
from LogSetup import logger
from ExecutionContext import ExecutionContext
from ConfigUtility import JsonConfigUtility
from MySQLConnectionManager import MySQLConnectionManager

MODULE_NAME = "ThresholdCheck"

"""
Module Name         :   ThresholdCheck
Purpose             :   This module will perform Data Quality Checks on DW tables.
Input Parameters    :   process id, frequency , process_name
Output Value        :   None
Pre-requisites      :   None
Last changed on     :   10th May 2021
Last changed by     :   Sukhwinder Singh
Reason for change   :
"""


class ThresholdCheck(object):
    def __init__(self):
        self.execution_context = ExecutionContext()
        self.execution_context.set_context({"module_name": MODULE_NAME})
        self.configuration = JsonConfigUtility(CommonConstants.AIRFLOW_CODE_PATH + '/' +
                                               CommonConstants.ENVIRONMENT_CONFIG_FILE)
        self.audit_db = self.configuration.get_configuration([CommonConstants.ENVIRONMENT_PARAMS_KEY, "mysql_db"])

    def dqm(self, spark, dqm_s3_detail_location, dqm_s3_summary_location, threshold, qc_query,
                 threshold_type, status_message, cycle_id, row):
        """
        Will compare the error records with threshold level and write the error records in S3
        :param spark: spark session
        :param dqm_s3_detail_location: S3 location for writing the error records in details
        :param dqm_s3_summary_location: S3 location for writing the error summary
        :param threshold: Threshold level for error
        :param qc_query: QC query to check the records having errors
        :param threshold_type: Type of the threshold level
        :param status_message: Status description for errors
        :param cycle_id: Cycle ID of process
        :param row: config details of DQM check
        :return:
        """
        detail_error_path = dqm_s3_detail_location
        summary_error_path = dqm_s3_summary_location
        status = CommonConstants.STATUS_FAILED
        record_count = 0
        error_percentage = None
        error_record_df = spark.sql(qc_query)

        try:
            if threshold is None or threshold == "":
                status_message = "There is no threshold configured for this check - hence DQM check is invalid"
                logger.error(status_message, extra=self.execution_context.get_context())
                raise Exception(status_message)
            else:
                record_count = error_record_df.count()
                status_message = "Count of error records: {0}".format(record_count)
                logger.info(status_message, extra=self.execution_context.get_context())
                dataset_id = str(row['dataset_id'])
                process_id = str(row['process_id'])
                total_count_query = "select record_count from {audit_db}.{process_dependency_table} where process_id = '{pid}' and dataset_id ='{did}' and write_dependency_value = '{wdv}'".format(
                    audit_db=self.audit_db, process_dependency_table=CommonConstants.PROCESS_DEPENDENCY_DETAILS_TABLE,
                    pid=process_id, did=dataset_id, wdv=cycle_id)
                record_count_res = MySQLConnectionManager().execute_query_mysql(total_count_query, True)
                total_record_count = record_count_res['record_count']
                record_count = error_record_df.count()
                error_percentage = (record_count / total_record_count) * 100
                error_percentage = round(error_percentage, 3)
                status_message = "Error percentage is : {0}".format(error_percentage)
                logger.info(status_message, extra=self.execution_context.get_context())

            detail_column_schema = StructType([StructField(CommonConstants.DQM_DW_CYCLE_ID, StringType(), False),
                                        StructField(CommonConstants.DQM_DW_DATASET_ID, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_QC_ID, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_QC_TYPE, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_QC_QUERY, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_ERROR_VALUE, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_CRITICALITY, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_CREATE_BY, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_CREATE_TS, StringType(), True),
                                        StructField(CommonConstants.DQM_DW_APP_ID, StringType(), True)])
            summary_column_schema = StructType([StructField(CommonConstants.DQM_DW_CYCLE_ID, StringType(), False),
                                               StructField(CommonConstants.DQM_DW_DATASET_ID, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_QC_ID, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_QC_TYPE, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_QC_QUERY, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_THRESHOLD_TYPE, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_THRESHOLD_VALUE, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_ERROR_COUNT, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_ERROR_PERCENTAGE, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_CRITICALITY, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_CREATE_BY, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_CREATE_TS, StringType(), True),
                                               StructField(CommonConstants.DQM_DW_APP_ID, StringType(), True)])

            if threshold_type.lower() == "count":
                status_message = "The threshold type is Count"
                logger.info(status_message, extra=self.execution_context.get_context())
                threshold_compare_type = record_count
            elif threshold_type.lower() == "percent":
                status_message = "The threshold type is Percentage"
                logger.info(status_message, extra=self.execution_context.get_context())
                threshold_compare_type = error_percentage
            else:
                status_message = "Threshold type configured is invalid. This should be 'count' or 'percent'"
                logger.error(status_message, extra=self.execution_context.get_context())
                raise Exception(status_message)

            if threshold_compare_type > threshold:
                status_message = "Error records are greater than threshold - {0}".format(threshold)
                logger.info(status_message, extra=self.execution_context.get_context())
                error_details_record = []
                error_summary_record = []
                summary_record = []
                dataframe_list = error_record_df.select('*').rdd.map(
                    lambda x: str([i for i in x]).replace("[", "").replace("]", "")).collect()
                for record in dataframe_list:
                    record_list = []
                    # creating a dataframe from the lists of data
                    record_list.append(cycle_id)
                    record_list.append(row[CommonConstants.DQM_DW_DATASET_ID])
                    record_list.append(row[CommonConstants.DQM_DW_QC_ID])
                    record_list.append(row[CommonConstants.DQM_DW_QC_TYPE])
                    record_list.append(row[CommonConstants.DQM_DW_QC_QUERY])
                    record_list.append(record)
                    record_list.append(row[CommonConstants.DQM_DW_CRITICALITY])
                    record_list.append(row[CommonConstants.DQM_DW_APP_ID])
                    record_list.append(row[CommonConstants.DQM_DW_CREATE_BY])
                    record_list.append(row[CommonConstants.DQM_DW_CREATE_TS])
                    error_details_record.append(record_list)
                # creating a summary dataframe
                summary_record.append(cycle_id)
                summary_record.append(row[CommonConstants.DQM_DW_DATASET_ID])
                summary_record.append(row[CommonConstants.DQM_DW_QC_ID])
                summary_record.append(row[CommonConstants.DQM_DW_QC_TYPE])
                summary_record.append(row[CommonConstants.DQM_DW_QC_QUERY])
                summary_record.append(threshold_type)
                summary_record.append(threshold)
                summary_record.append(record_count)
                summary_record.append(error_percentage)
                summary_record.append(row[CommonConstants.DQM_DW_CRITICALITY])
                summary_record.append(row[CommonConstants.DQM_DW_APP_ID])
                summary_record.append(row[CommonConstants.DQM_DW_CREATE_BY])
                summary_record.append(row[CommonConstants.DQM_DW_CREATE_TS])
                error_summary_record.append(summary_record)
                error_record_dataframe = spark.createDataFrame(error_details_record, detail_column_schema)
                error_record_dataframe.write.mode('overwrite').format("parquet").option("header", "true").save(
                    detail_error_path)
                error_summary_record_dataframe = spark.createDataFrame(error_summary_record, summary_column_schema)
                error_summary_record_dataframe.write.mode('overwrite').format("parquet").option("header", "true").save(
                    summary_error_path)
                status = CommonConstants.STATUS_FAILED
            elif threshold_compare_type >= 0 and threshold_compare_type <= threshold:
                status = CommonConstants.STATUS_SUCCEEDED

            summary_statement = str(record_count) + " records " + status + " " + str(status_message)

            result = {
                "validation_message": summary_statement,
                "error_record_count": record_count,
                "error_percentage": error_percentage,
                "validation_status": status,
                "error_details_path": detail_error_path
            }

            return result
        except Exception as ex:
            logger.error(str(ex), extra=self.execution_context.get_context())
            raise ex

